import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'site-search',
  templateUrl: './site-search.component.html',
  styleUrls: ['./site-search.component.scss']
})
export class SiteSearchComponent implements OnInit {

  @ViewChild("searchInput") searchInput: any;
  public siteSearch: string = "";
  private closeListener;
  constructor(private router: Router) { }

  ngOnInit() {
  }


  focusOrSubmitSearch() {
    if (this.siteSearch) {
      const searchParam = encodeURIComponent(this.siteSearch);
      this.siteSearch = "";

      this.router.navigate([`/search/${searchParam}`]);
    } else {
      this.searchInput.nativeElement.focus();
    }
  }
}
