import { TestBed, inject } from '@angular/core/testing';

import { ObjectUtilityService } from './object-utility.service';

describe('ObjectUtilityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ObjectUtilityService]
    });
  });

  it('should be created', inject([ObjectUtilityService], (service: ObjectUtilityService) => {
    expect(service).toBeTruthy();
  }));
});
