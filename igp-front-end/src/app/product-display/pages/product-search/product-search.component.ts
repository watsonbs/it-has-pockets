import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { ProductService } from '../../product-service/product.service';
import { Product } from '../../Product';

@Component({
  selector: 'product-search',
  templateUrl: './product-search.component.html',
  styleUrls: ['./product-search.component.scss']
})
export class ProductSearchComponent implements OnInit, OnDestroy {
  public searchTerms: string;
  public searchContext: any;
  private paramsSubscription: Subscription

  constructor(private activatedRoute: ActivatedRoute, private productService: ProductService) { }

  ngOnInit() {
    this.paramsSubscription = this.activatedRoute.params.subscribe(params => {
      this.searchTerms = decodeURIComponent(params['searchTerm']);
      const searchKeywords = this.searchTerms.split(" ");

      this.searchContext = {
        keywords: searchKeywords
      };
    });
  }

  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
  }

}
