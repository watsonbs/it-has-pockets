const chai = require('chai');
const expect = chai.expect;
const productValidator = require("./product-validator");

describe("productValidator", () => {
    it("returns false if a validated product does not have a title", () => {
        const mockProduct = {
            vendor: "Vendor",
            price: 1000,
            wasPrice: false,
            // title: "A title",
            link: "A link",
            rating: false,
            images: [
                "an image"
            ],
            descriptionHtml: false,
            categories: ['category']
        };

        expect(productValidator.isValidProduct(mockProduct)).to.equal(false, "Should have failed product with no title");
    });

    it("returns false if a validated product does not have a vendor", () => {
        const mockProduct = {
            // vendor: "Vendor",
            price: 1000,
            wasPrice: false,
            title: "A title",
            link: "A link",
            rating: false,
            images: [
                "an image"
            ],
            descriptionHtml: false,
            categories: ['category']
        };

        expect(productValidator.isValidProduct(mockProduct)).to.equal(false, "Should have failed product with no vendor");
    });

    it("returns false if a validated product does not have a price", () => {
        const mockProduct = {
            vendor: "Vendor",
            // price: 1000,
            wasPrice: false,
            title: "A title",
            link: "A link",
            rating: false,
            images: [
                "an image"
            ],
            descriptionHtml: false,
            categories: ['category']
        };

        expect(productValidator.isValidProduct(mockProduct)).to.equal(false, "Should have failed product with no price");
    });

    it("returns false if a validated product does not have a link", () => {
        const mockProduct = {
            vendor: "Vendor",
            price: 1000,
            wasPrice: false,
            title: "A title",
            // link: "A link",
            rating: false,
            images: [
                "an image"
            ],
            descriptionHtml: false,
            categories: ['category']
        };

        expect(productValidator.isValidProduct(mockProduct)).to.equal(false, "Should have failed product with no link");
    });

    it("returns false if a validated product does not have any images", () => {
        const mockProduct = {
            vendor: "Vendor",
            price: 1000,
            wasPrice: false,
            title: "A title",
            link: "A link",
            rating: false,
            images: [],
            descriptionHtml: false,
            categories: ['category']
        };

        expect(productValidator.isValidProduct(mockProduct)).to.equal(false, "Should have failed product with no images");
    });

    it("returns false if a validated product does not have an images property", () => {
        const mockProduct = {
            vendor: "Vendor",
            price: 1000,
            wasPrice: false,
            title: "A title",
            link: "A link",
            rating: false,
            // images: [],
            descriptionHtml: false,
            categories: ['category']
        };

        expect(productValidator.isValidProduct(mockProduct)).to.equal(false, "Should have failed product with no images property");
    });

    it("returns false if a validated product does not have a categories property", () => {
        const mockProduct = {
            vendor: "Vendor",
            price: 1000,
            wasPrice: false,
            title: "A title",
            link: "A link",
            rating: false,
            images: ['animage'],
            descriptionHtml: false,
            // categories: ['category']
        };

        expect(productValidator.isValidProduct(mockProduct)).to.equal(false, "Should have failed product with no categories property");
    });

    it("returns false if a validated product does not have any categories", () => {
        const mockProduct = {
            vendor: "Vendor",
            price: 1000,
            wasPrice: false,
            title: "A title",
            link: "A link",
            rating: false,
            images: ['animage'],
            descriptionHtml: false,
            categories: []
        };

        expect(productValidator.isValidProduct(mockProduct)).to.equal(false, "Should have failed product with no categories");
    });
});