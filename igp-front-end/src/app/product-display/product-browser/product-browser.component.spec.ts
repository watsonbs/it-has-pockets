import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxPaginationModule } from 'ngx-pagination';
import { IgpElementsModule } from '../../igp-elements/igp-elements.module';
import { ProductBrowserComponent } from './product-browser.component';
import { ProductBrowserService } from './product-browser.service';
import { ProductListComponent } from '../product-list/product-list.component';
import { ProductCardComponent } from '../product-card/product-card.component';
import { ProductFilterComponent } from '../product-filter/product-filter.component';
import { ProductFilterSummaryComponent } from '../product-filter-summary/product-filter-summary.component';
import { ProductService } from '../product-service/product.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

describe('ProductBrowserComponent', () => {
  let component: ProductBrowserComponent;
  let fixture: ComponentFixture<ProductBrowserComponent>;

  const mockProductList = [
    {
      "vendor": "boohoo",
      "price": 2500,
      "wasPrice": false,
      "title": "Lois Slouch Pocket Denim Dress",
      "link": "http://www.boohoo.com/lois-slouch-pocket-denim-dress/DZZ89277.html",
      "rating": false,
      "images": ["//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_large_mobile$"],
      "descriptionHtml": false,
      "categories": ["dresses"]
    }, {
      "vendor": "boohoo",
      "price": 2000,
      "wasPrice": false,
      "title": "Pansy Patch Pocket Denim Pinafore Dress",
      "link": "http://www.boohoo.com/pansy-patch-pocket-denim-pinafore-dress/DZZ63135.html",
      "rating": false,
      "images": ["//i1.adis.ws/i/boohooamplience/dzz63135_mid%2520blue_xl?$product_image_category_page_large_mobile$"],
      "descriptionHtml": false,
      "categories": ["dresses"]
    }
  ];
  class MockProductService {
    getProductList = jasmine.createSpy('getProductList').and.returnValue(Observable.of(mockProductList))
  }
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductBrowserComponent, ProductListComponent, ProductCardComponent, ProductFilterComponent, ProductFilterSummaryComponent],
      imports: [FormsModule, IgpElementsModule, NgxPaginationModule, RouterTestingModule],
      providers: [
        ProductBrowserService,
        { provide: ProductService, useClass: MockProductService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductBrowserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should expose the product list to the view', inject([ProductService], (productService) => {
    expect(component.productList).toBe(mockProductList);
  }));
});
