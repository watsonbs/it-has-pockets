import { TestBed, inject } from '@angular/core/testing';
import { ProductService } from './product.service';

import { HttpModule, Http, BaseRequestOptions, XHRBackend, Response, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';

describe('ProductService', () => {
  const mockProductList = [
    {
      "vendor": "boohoo",
      "price": 2500,
      "wasPrice": false,
      "title": "Lois Slouch Pocket Denim Dress",
      "link": "http://www.boohoo.com/lois-slouch-pocket-denim-dress/DZZ89277.html",
      "rating": false,
      "images": ["//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_large_mobile$"],
      "descriptionHtml": false,
      "categories": ["dresses"]
    }, {
      "vendor": "boohoo",
      "price": 2000,
      "wasPrice": false,
      "title": "Pansy Patch Pocket Denim Pinafore Dress",
      "link": "http://www.boohoo.com/pansy-patch-pocket-denim-pinafore-dress/DZZ63135.html",
      "rating": false,
      "images": ["//i1.adis.ws/i/boohooamplience/dzz63135_mid%2520blue_xl?$product_image_category_page_large_mobile$"],
      "descriptionHtml": false,
      "categories": ["dresses"]
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [ProductService, { provide: XHRBackend, useClass: MockBackend }]
    });
  });

  it('should be able to get a full product list', inject([ProductService, XHRBackend], (productService: ProductService, mockBackend) => {
    mockBackend.connections.subscribe((connection) => {
      connection.mockRespond(new Response(new ResponseOptions({
        body: JSON.stringify(mockProductList)
      })));
    });

    productService.getProductList().subscribe((productList) => {
      expect(productList).toEqual(mockProductList);
    });
  }));

  it('should be able to filter a product list by keyword', inject([ProductService, XHRBackend], (productService: ProductService, mockBackend) => {
    mockBackend.connections.subscribe((connection) => {
      connection.mockRespond(new Response(new ResponseOptions({
        body: JSON.stringify(mockProductList)
      })));
    });

    productService.getProductList({ keywords: ["lois"] }).subscribe((productList) => {
      expect(productList).toEqual([mockProductList[0]]);
    });
  }));
});
