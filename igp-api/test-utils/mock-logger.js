const sinon = require("sinon");

const mockLogger = {
    error: sinon.stub(),
    reset: () => {
        mockLogger.error.reset();
    }
};

module.exports = mockLogger;