const chai = require('chai')
    .use(require('sinon-chai'));

const sinon = require('sinon');
const expect = chai.expect;
const proxyquire = require("proxyquire");

const mockStorage = {
    put: sinon.stub()
};
const getProductListMock = sinon.stub();
const scrapeImagesMock = sinon.stub();

const mockProductList = [{
        isProduct: true
    },
    {
        isProduct: true
    }
];
const productListKey = "productList";

const dataAcquisitionLambda = proxyquire("./data-acquisition-lambda", {
    "./generate-product-list/generate-product-list": getProductListMock,
    "./scrape-images/scrape-images": scrapeImagesMock,
    "../utilities/storage": mockStorage

});

describe("data-acquisition-lambda", () => {
    beforeEach(() => {
        process.env.ProductListKey = productListKey;
    });

    afterEach(() => {
        mockStorage.put.reset();
        getProductListMock.reset();
        scrapeImagesMock.reset();
    });

    it("loads all product list data, scrapes related images and stores the list", () => {
        const callbackSpy = sinon.spy();
        getProductListMock.returns(Promise.resolve(mockProductList));
        scrapeImagesMock.returns(Promise.resolve(mockProductList));
        mockStorage.put.returns(Promise.resolve("putresponse"));

        return dataAcquisitionLambda.load(null, null, callbackSpy).then(() => {
            expect(callbackSpy).called;
            expect(getProductListMock).called;
            expect(scrapeImagesMock).calledWith(mockProductList);
            expect(mockStorage.put).calledWith(productListKey, mockProductList);
        });
    });


    describe("error handling", () => {
        const callbackSpy = sinon.spy();
        beforeEach(() => {
            getProductListMock.returns(Promise.resolve(mockProductList));
            scrapeImagesMock.returns(Promise.resolve(mockProductList));
            mockStorage.put.returns(Promise.resolve("putresponse"));
        });
        afterEach(() => {
            mockStorage.put.reset();
            getProductListMock.reset();
            scrapeImagesMock.reset();
            callbackSpy.reset();
        });

        it("calls the callback with an error if one is thrown getting the product List", () => {
            getProductListMock.reset();
            getProductListMock.returns(Promise.reject("get product error"));
            return dataAcquisitionLambda.load(null, null, callbackSpy).then(() => {
                expect(callbackSpy).calledWith("get product error")
            });
        });

        it("calls the callback with an error if one is thrown scraping the images", () => {
            scrapeImagesMock.reset();
            scrapeImagesMock.returns(Promise.reject("scrape images error"));
            return dataAcquisitionLambda.load(null, null, callbackSpy).then(() => {
                expect(callbackSpy).calledWith("scrape images error")
            });
        });

        it("calls the callback with an error if one is thrown storing the product list", () => {
            mockStorage.put.reset();
            mockStorage.put.returns(Promise.reject("scrape images error"));
            return dataAcquisitionLambda.load(null, null, callbackSpy).then(() => {
                expect(callbackSpy).calledWith("scrape images error")
            });
        });
    });
});