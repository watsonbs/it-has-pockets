const request = require("request");
const cheerio = require("cheerio");


const extractPriceAsPence = (priceString) => {
    let price = priceString.replace("£", "");
    price = parseFloat(price);
    return price * 100;
};

const getPrice = ($element) => {
    let price = $element.find(".price span").last().text();
    return extractPriceAsPence(price);
};

const getWasPrice = ($element) => {
    let wasPrice = $element.find(".was-price").last().text();
    if (!wasPrice) return false;

    return extractPriceAsPence(wasPrice);
};

const getTitle = ($element) => {
    return $element.find(".sli_list_title a").first().text();
}

const getLink = ($element) => {
    return $element.find(".sli_list_title a").first().attr("href");
};

const getRating = ($element) => {
    const value = parseFloat($element.find(".ratingDecimal").text());
    const count = parseInt($element.find(".ratingCount").text().replace("(", ""), 10);
    
    return {
        value: isNaN(value) ? null : value,
        count: isNaN(count) ? null : count
    };
};

const getImages = ($element) => {
    let images = [];
    const $primaryImage = $element.find(".sli_list_image img");
    images.push({
        src: $primaryImage.attr("src"),
        alt: $primaryImage.attr("alt")
    });

    $element.find(".swatch-small").each(((index, element) => {
        const image = element && element.attribs && element.attribs.rel;
        if (image && image !== images[0].src) {
            images.push({
                src: image,
                thumb: image,
                alt: $primaryImage.attr("alt")
            });
        }
    }));

    return images;


};

const getDescriptionHtml = ($element) => {
    return $element.find(".sli_list_content").children().first().html().replace(/\n/g, '').trim().replace(/\s+/g, ' ');
};

const getProductDataFromBodenPage = (body) => {
    let $ = cheerio.load(body);
    let items = [];
    const elements = $("#sli_products_wrapper").find(".sli_list_result");

    elements.each((index, element) => {
        const $element = $(element);
        let item = {
            vendor: 'boden',
            price: getPrice($element),
            wasPrice: getWasPrice($element),
            title: getTitle($element),
            link: getLink($element),
            rating: getRating($element),
            images: getImages($element),
            // descriptionHtml: getDescriptionHtml($element),
            categories: ['dresses']
        };
        items.push(item);
    });
    return items;
};

module.exports = () => {
    return new Promise((resolve, reject) => {
        request.get("http://clothing.boden.co.uk/clothing/Dresses-With-Pockets?af=cat1%3awomen", (error, response, body) => {
            if (error) throw error;

            const formattedData = getProductDataFromBodenPage(body);

            if (!formattedData.length) throw "No valid products were found";

            resolve(formattedData);
        });
    });
};