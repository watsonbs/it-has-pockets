import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IgpCheckBoxComponent } from './igp-checkbox/igp-checkbox.component';
import { IgpRadiobuttonComponent } from './igp-radiobutton/igp-radiobutton.component';
import { IgpAccordionRowComponent } from './igp-accordion-row/igp-accordion-row.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [IgpCheckBoxComponent, IgpRadiobuttonComponent, IgpAccordionRowComponent],
  declarations: [IgpCheckBoxComponent, IgpRadiobuttonComponent, IgpAccordionRowComponent]
})
export class IgpElementsModule { }
