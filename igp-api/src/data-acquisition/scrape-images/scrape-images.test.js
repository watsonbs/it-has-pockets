const chai = require('chai')
    .use(require('sinon-chai'));

const sinon = require('sinon');
const expect = chai.expect;
const proxyquire = require("proxyquire");

const mockRequest = require("../../../test-utils/mock-request");

const mockResizeImg = sinon.stub();

const mockStorage = {
    put: sinon.stub(),
    getBlob: sinon.stub()
};



const scrapeImages = proxyquire('./scrape-images', {
    "../../utilities/storage": mockStorage,
    "request": mockRequest,
    "resize-img": mockResizeImg
});

describe("scrapeImages", () => {
    const mockProductList = [{
            otherData: true,
            images: [{
                    "src": "image1.1",
                    "thumb": "thumb1.1"
                },
                {
                    "src": "image1.2"
                },
                {
                    "src": "image1.3"
                }
            ]
        },
        {
            images: [{
                "src": "image2.1"
            }]
        }
    ];
    const mockFileContent = "file";

    beforeEach(() => {
        mockRequest.mock.setResponse("get", null, null, mockFileContent);
        mockStorage.put.returns(Promise.resolve("putResponse"));
    });
    afterEach(() => {
        mockStorage.getBlob.reset();
        mockStorage.put.reset();
    });
    it("does not load images if we already have them stored", () => {
        mockStorage.getBlob.returns(Promise.resolve("some content"));

        return scrapeImages(mockProductList).then((updatedProductList) => {
            expect(updatedProductList).to.deep.equal([{
                    otherData: true,
                    images: [{
                            src: "ee74b507e51b613b256510c0115c24f4",
                            thumb: "e92c1d9b164b797c32fdb267964d596b"
                        },
                        {
                            src: "2d818105cda16d52d2461989f3eb8cef",
                        },
                        {
                            src: "352bc1f0d089919f208a6d4567257a6a"
                        }
                    ]
                },
                {
                    images: [{
                        "src": "44e3c5dce329fd02a55de801c29143ae"
                    }]
                }
            ]);
        });
    });
    it("gets all images and stores them with a hash of their original url as the key", () => {
        mockStorage.getBlob.returns(Promise.reject({
            code: "NoSuchKey"
        }));

        return scrapeImages(mockProductList).then(() => {
            expect(mockStorage.put).calledWith("ee74b507e51b613b256510c0115c24f4", mockFileContent);
            expect(mockStorage.put).calledWith("2d818105cda16d52d2461989f3eb8cef", mockFileContent);
            expect(mockStorage.put).calledWith("352bc1f0d089919f208a6d4567257a6a", mockFileContent);
            expect(mockStorage.put).calledWith("44e3c5dce329fd02a55de801c29143ae", mockFileContent);
        });
    });

    it("returns a product list with all image urls converted to a key", () => {
        mockStorage.getBlob.returns(Promise.reject({
            code: "NoSuchKey"
        }));

        return scrapeImages(mockProductList).then((updatedProductList) => {
            expect(updatedProductList).to.deep.equal([{
                    otherData: true,
                    images: [{
                            src: "ee74b507e51b613b256510c0115c24f4",
                            thumb: "e92c1d9b164b797c32fdb267964d596b"
                        },
                        {
                            src: "2d818105cda16d52d2461989f3eb8cef",
                        },
                        {
                            src: "352bc1f0d089919f208a6d4567257a6a"
                        }
                    ]
                },
                {
                    images: [{
                        "src": "44e3c5dce329fd02a55de801c29143ae"
                    }]
                }
            ]);
        });
    });

    it("resizes images", () => {

    });

    describe("missing images", () => {
        beforeEach(() => {
            mockStorage.getBlob.returns(Promise.reject({
                code: "NoSuchKey"
            }));
        });
        it("removes images which cannot be downloaded from the resultant products", () => {
            mockRequest.get.reset();
            mockRequest.get
                .onFirstCall().callsArgWith(1, "error", null, null)
                .callsArgWith(1, null, null, mockFileContent)

            return scrapeImages(mockProductList).then((updatedProductList) => {
                expect(updatedProductList).to.deep.equal([{
                        otherData: true,
                        images: [{
                                src: "2d818105cda16d52d2461989f3eb8cef",
                            },
                            {
                                src: "352bc1f0d089919f208a6d4567257a6a"
                            }
                        ]
                    },
                    {
                        images: [{
                            src: "44e3c5dce329fd02a55de801c29143ae"
                        }]
                    }
                ])
            });
        });

        it("removes images which cannot be stored from the resultant products", () => {
            mockStorage.put.reset();
            mockStorage.put
                .onFirstCall().returns(Promise.reject("error"))
                .returns(Promise.resolve("putResponse"))

            return scrapeImages(mockProductList).then((updatedProductList) => {
                expect(updatedProductList).to.deep.equal([{
                        otherData: true,
                        images: [{
                                src: "2d818105cda16d52d2461989f3eb8cef",
                            },
                            {
                                src: "352bc1f0d089919f208a6d4567257a6a"
                            }
                        ]
                    },
                    {
                        images: [{
                            src: "44e3c5dce329fd02a55de801c29143ae"
                        }]
                    }
                ])
            });
        });

        it("removes products which do not have any images at the end", () => {
            mockRequest.get.reset();
            mockRequest.get
                .onCall(4).callsArgWith(1, "error", null, null)
                .callsArgWith(1, null, null, mockFileContent)

            return scrapeImages(mockProductList).then((updatedProductList) => {
                expect(updatedProductList).to.deep.equal([{
                    otherData: true,
                    images: [{
                            src: "ee74b507e51b613b256510c0115c24f4",
                            thumb: "e92c1d9b164b797c32fdb267964d596b"
                        },
                        {
                            src: "2d818105cda16d52d2461989f3eb8cef",
                        },
                        {
                            src: "352bc1f0d089919f208a6d4567257a6a"
                        }
                    ]
                }]);
            });
        });
    });
});