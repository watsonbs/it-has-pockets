import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { RouterModule } from '@angular/router';

import { ProductListComponent } from './product-list/product-list.component';
import { ProductService } from './product-service/product.service';
import { ProductCardComponent } from './product-card/product-card.component';
import { CurrencyPipeModule } from '../utilities/currency-pipe/currency-pipe.module';
import { IgpElementsModule } from '../igp-elements/igp-elements.module';
import { ProductSearchComponent } from './pages/product-search/product-search.component';
import { ProductHomeComponent } from './pages/product-home/product-home.component';
import { ProductFilterComponent } from './product-filter/product-filter.component';
import { ProductFilterSummaryComponent } from './product-filter-summary/product-filter-summary.component';
import { ProductBrowserComponent } from './product-browser/product-browser.component';
import { ProductBrowserService } from './product-browser/product-browser.service';
import { ObjectUtilityModule } from '../utilities/object-utility/object-utility.module';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    IgpElementsModule,
    CurrencyPipeModule,
    FormsModule,
    NgxPaginationModule,
    ObjectUtilityModule,
    RouterModule
  ],
  providers: [ProductService, ProductBrowserService],
  exports: [ProductListComponent],
  declarations: [ProductListComponent, ProductCardComponent, ProductSearchComponent, ProductHomeComponent, ProductFilterComponent, ProductFilterSummaryComponent, ProductBrowserComponent]
})
export class ProductDisplayModule { }
