import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductFilterSummaryComponent } from './product-filter-summary.component';

xdescribe('ProductFilterSummaryComponent', () => {
  let component: ProductFilterSummaryComponent;
  let fixture: ComponentFixture<ProductFilterSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductFilterSummaryComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductFilterSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
