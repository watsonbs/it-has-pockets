const chai = require('chai')
    .use(require('sinon-chai'));

const sinon = require('sinon');
const expect = chai.expect;
const proxyquire = require("proxyquire");

const mockGetBoohoo = sinon.stub();
const mockGetBoden = sinon.stub();
const mockLogger = require("../../../test-utils/mock-logger");

const generateProductList = proxyquire('./generate-product-list', {
    '../product-scrapers/get-boohoo/get-boohoo': mockGetBoohoo,
    '../product-scrapers/get-boden/get-boden': mockGetBoden,
    '../../utilities/logger': mockLogger
});

describe("getProductList", () => {
    afterEach(() => {
        mockGetBoden.reset();
        mockGetBoohoo.reset();
        mockLogger.reset();
    });

    it("gets and concatenates product list data from boohoo and boden", () => {
        mockGetBoohoo.returns(Promise.resolve(['boohoo1', 'boohoo2']));
        mockGetBoden.returns(Promise.resolve(['boden1', 'boden2']));

        return generateProductList().then((productList) => {
            expect(mockGetBoden).called;
            expect(mockGetBoohoo).called;
            expect(productList).to.deep.equal([
                'boohoo1',
                'boohoo2',
                'boden1',
                'boden2'
            ]);
        });
    });

    it("logs an error but returns the rest of the data if boden throws an error", () => {
        mockGetBoohoo.returns(Promise.resolve(['boohoo1', 'boohoo2']));
        mockGetBoden.returns(Promise.reject("biffed"));

        return generateProductList().then((productList) => {
            expect(mockGetBoden).called;
            expect(mockGetBoohoo).called;
            expect(mockLogger.error).calledWith("boden", "biffed");
            expect(productList).to.deep.equal([
                'boohoo1',
                'boohoo2'
            ]);
        });
    });
});