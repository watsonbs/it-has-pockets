const request = require("request");
const cheerio = require("cheerio");
const productValidator = require("../../validators/product-validator");

const extractPriceAsPence = (priceString) => {
    let price = priceString.replace("£", "");
    price = parseFloat(price);
    return price * 100;
};

const getPrice = ($element) => {
    let price = $element.find(".product-sales-price").last().text();
    return extractPriceAsPence(price);
};

const getWasPrice = ($element) => {
    let wasPrice = $element.find(".product-standard-price").last().text();
    if (!wasPrice) return false;

    return extractPriceAsPence(wasPrice);
};

const getTitle = ($element) => {
    return $element.find(".name-link").first().text().replace(/\n/g, '').trim();
}

const getLink = ($element) => {
    return $element.find(".name-link").first().attr("href");
};

const getRating = ($element) => {
    return false;
};

const isValidImage = (image) => {
    return image.src && image.thumb;
}

const getImages = ($element, $) => {
    let images = [];
    const productSwatch = $element.find(".product-swatch-item");

    if (!productSwatch || !productSwatch.length) {
        images.push({
            src: $element.find(".js-ampliance-desktop").attr("srcset"),
            alt: $element.find("img").attr("alt")
        });
        return images;
    }

    productSwatch.each(((index, productSwatch) => {
        if(!productSwatch) return;

        const $productSwatch = $(productSwatch);

        let variantLink = $productSwatch.find(".swatch").attr("href");
        let imageElement = $productSwatch.find(".swatch-image");
        let imageSet = imageElement.data("thumb");

        const image = {
            thumb: imageElement.attr("src"),
            src: imageSet.src,
            alt: imageElement.attr("alt"),
            variantLink
        };
        

        if (isValidImage(image)) {
            images.push(image);
        }
    }));

    return images;
};

const getDescriptionHtml = ($element) => {
    return false;
};

const getProductDataFromBoohooPage = (body) => {
    let $ = cheerio.load(body);
    let items = [];
    const elements = $("#search-result-items").find(".product-tile");


    elements.each((index, element) => {
        const $element = $(element);
        let item = {
            vendor: 'boohoo',
            price: getPrice($element),
            wasPrice: getWasPrice($element),
            title: getTitle($element),
            link: getLink($element),
            rating: getRating($element),
            images: getImages($element, $),
            descriptionHtml: getDescriptionHtml($element),
            categories: ['dresses']
        };
        if (productValidator.isValidProduct(item)) {
            items.push(item);
        }
    });
    return items;
};

module.exports = () => {
    return new Promise((resolve, reject) => {
        request.get("http://www.boohoo.com/search?q=pockets&prefn1=category&prefn2=gender&prefv1=Dresses&prefv2=Female", (error, response, body) => {
            if (error) return reject(error);

            const formattedData = getProductDataFromBoohooPage(body);

            if (!formattedData.length) return reject("No valid products were found");

            return resolve(formattedData);
        });
    });
};