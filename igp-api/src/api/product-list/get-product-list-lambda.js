const storage = require("../../utilities/storage");

exports.get = (event, context, callback) => {
    return storage.getJSON(process.env.ProductListKey)
        .then((productList) => {
            callback(null, {
                "statusCode": 200,
                "headers": {
                    "Content-Type": "application/json",
                    "Cache-Control": "private, max-age=86400000"
                },
                "body": {
                    data: productList
                }
            });
        })
        .catch((error) => {
            error.statusCode = error.statusCode || 500;
            callback(error);
        })
}