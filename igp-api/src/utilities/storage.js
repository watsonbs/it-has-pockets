const AWS = require("aws-sdk");

const isJSON = (content) => {
    return content && (Array.isArray(content) || (typeof content === 'object'));
};
const createBuffer = (content) => {
    const contentString = isJSON(content) ? JSON.stringify(content) : content.toString();
    return new Buffer(contentString, 'utf-8');
};

const storage = {
    _init: () => {
        this.S3 = new AWS.S3({
            apiVersion: '2006-03-01'
        });
        this.ready = true;
    },
    put: (key, fileData) => {
        if (!this.ready) storage._init();

        return new Promise((resolve, reject) => {
            this.S3.putObject({
                Key: key,
                Bucket: process.env.BucketName,
                Body: createBuffer(fileData)
            }, (error, data) => {
                if (error) return reject(error);
                return resolve(data);
            });
        });
    },
    _get: (key) => {
        if (!this.ready) storage._init();

        return new Promise((resolve, reject) => {
            this.S3.getObject({
                Key: key,
                Bucket: process.env.BucketName
            }, (error, data) => {
                if (error) reject(error);
                return resolve(data.Body.toString());
            });
        });
    },
    getBlob: (key) => {
        return storage._get(key);
    },
    getJSON: (key) => {
        return storage._get(key).then((responseBody) => {
            return JSON.parse(responseBody)
        });
    }
}

module.exports = storage;