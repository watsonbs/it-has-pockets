import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'igp-radiobutton',
  templateUrl: './igp-radiobutton.component.html',
  styleUrls: ['./igp-radiobutton.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => IgpRadiobuttonComponent),
      multi: true
    }
  ]
})
export class IgpRadiobuttonComponent implements OnInit {
  @Input() name: string;
  @Input() label: string;
  @Input() value: string;
  @Input() ngModelOptions: any;

  public id: string;
  public isChecked: boolean;

  private propagateChange = (_: any) => { };
  private propagateTouched = (_: any) => { };


  constructor() { }

  ngOnInit() {
    this.id = `${this.name}_${this.value}`;
  }

  writeValue(value: any) {
    this.isChecked = (value === this.value);
  }
  registerOnChange(fn) { this.propagateChange = fn }
  registerOnTouched(fn) { this.propagateTouched = fn }
  onChange(event) {
    this.propagateChange(event.target.value);
  }
}

