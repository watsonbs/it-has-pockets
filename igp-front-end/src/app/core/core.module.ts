import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { SiteHeaderComponent } from './site-header/site-header.component';
import { SiteSearchComponent } from './site-search/site-search.component';
import { SiteFooterComponent } from './site-footer/site-footer.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  exports: [SiteHeaderComponent, SiteFooterComponent],
  declarations: [SiteHeaderComponent, SiteSearchComponent, SiteFooterComponent]
})
export class CoreModule { }
