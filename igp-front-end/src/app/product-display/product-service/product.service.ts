import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Product } from '../Product';


@Injectable()
export class ProductService {
  private flagTests = {
    "isDiscounted": (product) => Boolean(product.wasPrice)
  };
  private sortTests = {
    "price_d": (a, b) => b.price - a.price,
    "price_a": (a, b) => a.price - b.price
  }

  constructor(private http: Http) { }

  private matchesFlags(product, flags) {
    if (!flags || !Object.keys(flags).length) return true;

    return Object.keys(flags).reduce((isValid, flagName) => {
      if (!flags[flagName]) return true;
      return isValid && this.flagTests[flagName](product);
    }, true);

  }
  private withinPriceRange(product, minPrice, maxPrice) {
    let inRange = true;
    if (minPrice) {
      inRange = product.price >= (minPrice * 100);
    }
    if (maxPrice && inRange) {
      inRange = product.price <= (maxPrice * 100);
    }
    return inRange;
  }
  private containsKeywords(product, keywords) {
    if (!keywords || !keywords.length) return true;
    if (!Array.isArray(keywords)) keywords = [keywords];

    return keywords.reduce((isValid, keyword) => {
      return isValid && product.title.toLowerCase().indexOf(keyword.toLowerCase()) > -1;
    }, true);
  }
  getProductList(search?, sort?: string): Observable<Product[]> {
    return this.http.get("/assets/mock-products.json").map(
      (res) => {
        return res.json();
      }
    ).map((products) => {
      if (!search) return products;

      let filtererdProducts = products.filter((product) => {
        return this.containsKeywords(product, search.keywords) &&
          this.withinPriceRange(product, search.minPrice, search.maxPrice) &&
          this.matchesFlags(product, search.flags)
      });

      const sortTest = this.sortTests[sort];
      if (sortTest) {
        filtererdProducts = filtererdProducts.sort(sortTest);
      }


      return filtererdProducts;
    });
  }
  getCategory(key) {
    // return Observable.of({ key: "dresses", label: "Dresses" });
    return { key: "dresses", label: "Dresses" };
  }

}
