import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IgpAccordionRowComponent } from './igp-accordion-row.component';

describe('IgpAccordionRowComponent', () => {
  let component: IgpAccordionRowComponent;
  let fixture: ComponentFixture<IgpAccordionRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IgpAccordionRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IgpAccordionRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
