import { async, ComponentFixture, TestBed, tick } from '@angular/core/testing';

import { IgpCheckBoxComponent } from './igp-checkbox.component';

describe('CheckBoxComponent', () => {
  let component: IgpCheckBoxComponent;
  let fixture: ComponentFixture<IgpCheckBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IgpCheckBoxComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IgpCheckBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
