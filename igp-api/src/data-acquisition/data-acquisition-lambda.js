const generateProductList = require("./generate-product-list/generate-product-list");
const scrapeImages = require("./scrape-images/scrape-images");
const storage = require("../utilities/storage");

const storeProductList = (productList) => {
    return storage.put(process.env.ProductListKey, productList);
};

exports.load = (event, context, callback) => {
    return generateProductList()
        .then(scrapeImages)
        .then(storeProductList)
        .then(callback())
        .catch((error) => callback(error));
};