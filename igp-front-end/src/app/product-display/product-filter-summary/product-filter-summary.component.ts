import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'product-filter-summary',
  templateUrl: './product-filter-summary.component.html',
  styleUrls: ['./product-filter-summary.component.scss']
})
export class ProductFilterSummaryComponent implements OnInit, OnChanges {

  @Input() private filterQuery;
  @Input() resultCount: number;
  @Input() pageSize: number;

  public descriptionSections: {
    categories?: string;
    price?: string;
    keywords?: string;
    pageSize?: string;
  }
  constructor() { }

  private stringifyList(list) {
    return [list.slice(0, -1).join(', '), list.slice(-1)[0]].join(list.length < 2 ? '' : ' and ');
  }

  private generatePriceDescriptionString(minPrice, maxPrice, flags) {
    let priceDescriptionString;

    if (minPrice) {
      if (this.filterQuery.maxPrice) {
        priceDescriptionString = `between £${minPrice} and £${maxPrice}`;
      } else {
        priceDescriptionString = `more than £${minPrice}`
      }
    } else if (maxPrice) {
      priceDescriptionString = `less than £${maxPrice}`
    }

    if (flags.isDiscounted) {
      if (priceDescriptionString) {
        priceDescriptionString = `discounted to ${priceDescriptionString}`;
      } else {
        priceDescriptionString = "which have been discounted"
      }
    } else if (priceDescriptionString) {
      priceDescriptionString = `costing ${priceDescriptionString}`;
    }

    return priceDescriptionString
  }
  private generateKeywordsDescriptionString(keywords, flags) {
    let keywordsDescription = '';
    if (keywords && keywords.length) {
      keywordsDescription = `containing keywords: ${this.stringifyList(keywords)}`;
    }
    return keywordsDescription;
  }
  private generateCategoriesDescriptionString(categories, flags) {
    let categoriesDescription = "items";
    if (categories && categories.length) {
      categoriesDescription = this.stringifyList(categories);
    }

    return categoriesDescription;
  }
  private generateSearchDescriptionStrings() {

    if (this.filterQuery) {
      const flags = this.filterQuery.flags;

      const categories = this.generateCategoriesDescriptionString(this.filterQuery.categories, flags);
      const keywords = this.generateKeywordsDescriptionString(this.filterQuery.keywords, flags);
      const price = this.generatePriceDescriptionString(this.filterQuery.minPrice, this.filterQuery.maxPrice, flags);

      this.descriptionSections = {
        categories,
        keywords,
        price
      };
    }

    this.descriptionSections = this.descriptionSections || {};

    this.descriptionSections.pageSize = (this.resultCount > this.pageSize) ? `up to ${this.pageSize} of` : "";
  }
  ngOnInit() {
    this.generateSearchDescriptionStrings();
  }

  ngOnChanges() {
    this.generateSearchDescriptionStrings();
  }

}
