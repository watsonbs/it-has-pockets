export class ProductFilters {
    categories?: any[]
    minPrice?: number
    maxPrice?: number
    keywords?: string[]
    flags?: any
};