import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../Product';

@Component({
  selector: 'product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {
  public activeImage: number = 0;

  @Input() product: Product;

  constructor() { }

  ngOnInit() {
  }

  changeImage(index) {
    this.activeImage = index;
  }

}
