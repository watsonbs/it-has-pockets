import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IgpRadiobuttonComponent } from './igp-radiobutton.component';

describe('IgpRadiobuttonComponent', () => {
  let component: IgpRadiobuttonComponent;
  let fixture: ComponentFixture<IgpRadiobuttonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IgpRadiobuttonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IgpRadiobuttonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
