const chai = require('chai')
    .use(require('sinon-chai'));

const sinon = require('sinon');
const expect = chai.expect;
const fs = require("fs");
const proxyquire = require("proxyquire");
const mockRequest = require("../../../../test-utils/mock-request");
const cheerio = require("cheerio");
const mockFullBodenPage = fs.readFileSync("./src/data-acquisition/product-scrapers/get-boden/mocks/mock-body.html");

const getBoden = proxyquire('./get-boden', {
    request: mockRequest
});

const failTest = () => {
    expect(true).to.equal(false, "Test reached an invalid state. A promise probably threw an unexpected error");
}

describe("getBoden", () => {
    afterEach(() => {
        mockRequest.mock.reset();
    });

    it("requests the boden dresses with pockets page", () => {
        sinon.spy(cheerio, "load")
        const stubResponse = `
            <div id="sli_products_wrapper">
                <div data-sli-test="resultitem" id="WW140" class="sli_list_result product-item">
                <div class="product-image sli_list_image">

                    <a target="_self" href="http://www.boden.co.uk/en-gb/womens-dresses/day-dresses/ww140/womens-laura-denim-shirt-dress" onclick="window.location.href='http://clothing.boden.co.uk/search?p=R&srid=S4-2LON1P&lbc=boden&w=Dresses%20With%20Pockets&url=http%3a%2f%2fwww.boden.co.uk%2fen-gb%2fwomens-dresses%2fday-dresses%2fww140%2fwomens-laura-denim-shirt-dress&lgsku=WW140&rk=7&uid=940632930&sid=2&ts=custom&champclick=1&SLIPid=1508094063384&rsc=Bn4h5EIZ%3aG0NHNPL&method=and&af=cat1%3awomen&isort=score&view=list&parturl=http%3a%2f%2fwww.boden.co.uk%2fen-gb%2fwomens-dresses%2fday-dresses%2fww140%2fwomens-laura-denim-shirt-dress';"
                        rel="nofollow">
                        <img class="img-responsive" pid="" src="http://www.bodenimages.com/productimages/productmedium/17waut_ww140_lbl_w01.jpg"
                            alt="Laura Denim Shirt Dress" title="Laura Denim Shirt Dress">
                    </a>
                </div>

                <div class="sli_list_content">
                    <div style="float:right;width:390px;padding-left: 5px;">
                        <p>Our button-through denim
                            <b>dress</b> is a real little masterpiece ÔÇô
                            <b>with</b> metal buttons and removable belt it certainly ticks all the
                            boxes. Fitted but
                            <b>with</b> a little added extra stretch for easy movement. Running from
                            the office to the bar just got a little easier.</p>
                        <div id="pm-bulletPoints" class="pdpProductDetails">
                            <ul>
                                <li>99% cotton 1% elastane</li>
                                <li>Machine washable</li>
                                <li>Flattering shape through bust and waist leading to a gentle A-line
                                    shape
                                </li>
                                <li>Length finishes below knee</li>
                                <li>Two front pockets</li>
                                <li>Full length sleeves with functional cuffs</li>
                                <li>Unlined</li>
                                <li>Button fastening at front</li>
                                <li>Self fabric tie belt at waist </li>
                            </ul>
                        </div>
                    </div>
                    <h2 class="sli_list_title sli_h2">
                        <a data-sli-test="resultlink" href="http://www.boden.co.uk/en-gb/womens-dresses/day-dresses/ww140/womens-laura-denim-shirt-dress"
                            onclick="window.location.href='http://clothing.boden.co.uk/search?p=R&srid=S4-2LON1P&lbc=boden&w=Dresses%20With%20Pockets&url=http%3a%2f%2fwww.boden.co.uk%2fen-gb%2fwomens-dresses%2fday-dresses%2fww140%2fwomens-laura-denim-shirt-dress&lgsku=WW140&rk=7&uid=940632930&sid=2&ts=custom&champclick=1&SLIPid=1508094063384&rsc=Bn4h5EIZ%3aG0NHNPL&method=and&af=cat1%3awomen&isort=score&view=list&parturl=http%3a%2f%2fwww.boden.co.uk%2fen-gb%2fwomens-dresses%2fday-dresses%2fww140%2fwomens-laura-denim-shirt-dress';"
                            title="http://www.boden.co.uk/en-gb/womens-dresses/day-dresses/ww140/womens-laura-denim-shirt-dress"
                            rel="nofollow">Laura Denim Shirt Dress</a>
                    </h2>

                    <p class="price">
                        <span class="ng-scope">
                            <span class="ng-scope">
                                <span class="ng-scope">

                                    <span class="was-price-section">
                                        <span class="was-price-text">
                                            <span class="was-price">&pound;90.00</span>
                                        </span>
                                    </span>
                                    <span>&pound;63.00</span>
                                </span>
                            </span>
                        </span>
                    </p>

                    <link rel="stylesheet" media="all" type="text/css" href="http://www.bodenimages.com/productimages/spritesw/_sprite_WW140.css">
                    <ul class="list-inline list-unstyled margin-left-none ng-scope">
                        <li class="swatch-small ng-scope" rel='http://www.bodenimages.com/productimages/productmedium/17waut_ww140_lbl.jpg' pid='#WW140'>
                            <a title="Bright Blue Wash" href="http://clothing.boden.co.uk/search?p=R&srid=S4-2LON1P&lbc=boden&w=Dresses%20With%20Pockets&url=http%3a%2f%2fwww.boden.co.uk%2fen-gb%2fwomens-dresses%2fday-dresses%2fww140%2fwomens-laura-denim-shirt-dress&lgsku=WW140&rk=7&uid=940632930&sid=2&ts=custom&champclick=1&SLIPid=1508094063384&rsc=Bn4h5EIZ%3aG0NHNPL&method=and&af=cat1%3awomen&isort=score&view=list&parturl=http%3a%2f%2fwww.boden.co.uk%2fen-gb%2fwomens-dresses%2fday-dresses%2fww140-lbl%2fwomens-bright-blue-wash-laura-denim-shirt-dress"
                                class="icon-WW140_LBL"></a>
                        </li>
                        <li class="swatch-small ng-scope" rel='http://www.bodenimages.com/productimages/productmedium/17waut_ww140_blu.jpg' pid='#WW140'>
                            <a title="Rinse Indigo" href="http://clothing.boden.co.uk/search?p=R&srid=S4-2LON1P&lbc=boden&w=Dresses%20With%20Pockets&url=http%3a%2f%2fwww.boden.co.uk%2fen-gb%2fwomens-dresses%2fday-dresses%2fww140%2fwomens-laura-denim-shirt-dress&lgsku=WW140&rk=7&uid=940632930&sid=2&ts=custom&champclick=1&SLIPid=1508094063384&rsc=Bn4h5EIZ%3aG0NHNPL&method=and&af=cat1%3awomen&isort=score&view=list&parturl=http%3a%2f%2fwww.boden.co.uk%2fen-gb%2fwomens-dresses%2fday-dresses%2fww140-lbl%2fwomens-bright-blue-wash-laura-denim-shirt-dress"
                                class="icon-WW140_BLU"></a>
                        </li>
                    </ul>

                    <div class="productRatingWrapper ng-scope">
                        <div class="ratingContainer">
                            <div class="rating sm-4-5 pull-left"></div>
                            <span class="ratingDecimal pull-left margin-left-sm ng-binding">4.7</span>

                        </div>
                        <span class="ratingCount ng-binding">(39 reviews)</span>
                    </div>


                    <div class="clearAll"></div>

                    <p class="sli_related_searches">Related Products:
                        <a class="urlkeylink" href="http://clothing.boden.co.uk/clothing/Denim-Dress">Denim Dress</a> |
                        <a class="urlkeylink" href="http://clothing.boden.co.uk/clothing/Denim-Shirt-Dress">Denim Shirt Dress</a> |
                        <a class="urlkeylink" href="http://clothing.boden.co.uk/clothing/Shirt-Dress">Shirt Dress</a>
                    </p>

                </div>



                <div class="sli_clear"></div>
            </div>
        </div>`;
        const dressesWithPocketsUrl = "http://clothing.boden.co.uk/clothing/Dresses-With-Pockets?af=cat1%3awomen";
        mockRequest.mock.setResponse("get", null, null, stubResponse);

        return getBoden().then(() => {
            expect(mockRequest.get).calledWith(dressesWithPocketsUrl);
            expect(cheerio.load).calledWith(stubResponse);
            cheerio.load.restore();
        }).catch(() => {
            cheerio.load.restore();
            failTest();
        });

    });

    it("parses the page into IGP product data", () => {
        mockRequest.mock.setResponse("get", null, null, mockFullBodenPage);

        return getBoden().then((bodenData) => {
            expect(bodenData.length).to.equal(37);
            expect(bodenData).to.deep.equal([
                {
                    "vendor": "boden",
                    "price": 6000,
                    "wasPrice": false,
                    "title": "Jessie Jersey Dress",
                    "link": "http://www.boden.co.uk/en-gb/womens-dresses/day-dresses/j0031/womens-jessie-jersey-dress",
                    "rating": {
                        "value": 4.5,
                        "count": 54
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0031_nvy_w01.jpg",
                            "alt": "Jessie Jersey Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0031_prp.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0031_prp.jpg",
                            "alt": "Jessie Jersey Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_j0031_dbl.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_j0031_dbl.jpg",
                            "alt": "Jessie Jersey Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0031_nvy.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0031_nvy.jpg",
                            "alt": "Jessie Jersey Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_j0031_nav.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_j0031_nav.jpg",
                            "alt": "Jessie Jersey Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_j0031_red.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_j0031_red.jpg",
                            "alt": "Jessie Jersey Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 7000,
                    "wasPrice": false,
                    "title": "Alda Dress",
                    "link": "http://www.boden.co.uk/en-gb/womens-dresses/day-dresses/w0019/womens-alda-dress",
                    "rating": {
                        "value": 4.1,
                        "count": 15
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0019_blk_w01.jpg",
                            "alt": "Alda Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0019_blk.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0019_blk.jpg",
                            "alt": "Alda Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_w0019_gry.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_w0019_gry.jpg",
                            "alt": "Alda Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0019_grn.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0019_grn.jpg",
                            "alt": "Alda Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_w0019_nvy.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_w0019_nvy.jpg",
                            "alt": "Alda Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_w0019_nnv.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_w0019_nnv.jpg",
                            "alt": "Alda Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 7000,
                    "wasPrice": false,
                    "title": "Hannah Jersey Dress",
                    "link": "http://www.boden.co.uk/en-gb/womens-dresses/day-dresses/j0018/womens-hannah-jersey-dress",
                    "rating": {
                        "value": 4.5,
                        "count": 70
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0018_red_w01.jpg",
                            "alt": "Hannah Jersey Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_j0018_blk.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_j0018_blk.jpg",
                            "alt": "Hannah Jersey Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_j0018_den.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_j0018_den.jpg",
                            "alt": "Hannah Jersey Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_j0018_dgr.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_j0018_dgr.jpg",
                            "alt": "Hannah Jersey Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_j0018_nav.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_j0018_nav.jpg",
                            "alt": "Hannah Jersey Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_j0018_red.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_j0018_red.jpg",
                            "alt": "Hannah Jersey Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_j0018_dyl.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_j0018_dyl.jpg",
                            "alt": "Hannah Jersey Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 9000,
                    "wasPrice": false,
                    "title": "Julianna Ponte Dress",
                    "link": "http://www.boden.co.uk/en-gb/womens-dresses/smart-day-dresses/j0021/womens-julianna-ponte-dress",
                    "rating": {
                        "value": 4.1,
                        "count": 26
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_j0021_brd_w01.jpg",
                            "alt": "Julianna Ponte Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_j0021_blk.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_j0021_blk.jpg",
                            "alt": "Julianna Ponte Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_j0021_grn.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_j0021_grn.jpg",
                            "alt": "Julianna Ponte Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_j0021_nav.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_j0021_nav.jpg",
                            "alt": "Julianna Ponte Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_j0021_brd.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_j0021_brd.jpg",
                            "alt": "Julianna Ponte Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 6500,
                    "wasPrice": false,
                    "title": "Flocked Spot Sweatshirt Dress",
                    "link": "http://www.boden.co.uk/en-gb/womens-dresses/day-dresses/j0037/womens-flocked-spot-sweatshirt-dress",
                    "rating": {
                        "value": 3.9,
                        "count": 10
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0037_pnt_w01.jpg",
                            "alt": "Flocked Spot Sweatshirt Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0037_pnt.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0037_pnt.jpg",
                            "alt": "Flocked Spot Sweatshirt Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0037_nvy.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0037_nvy.jpg",
                            "alt": "Flocked Spot Sweatshirt Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 8000,
                    "wasPrice": false,
                    "title": "Justine Jacquard Dress",
                    "link": "http://www.boden.co.uk/en-gb/womens-dresses/day-dresses/j0080/womens-justine-jacquard-dress",
                    "rating": {
                        "value": 4.3,
                        "count": 4
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0080_blk_w02.jpg",
                            "alt": "Justine Jacquard Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0080_blk.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0080_blk.jpg",
                            "alt": "Justine Jacquard Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0080_nav.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0080_nav.jpg",
                            "alt": "Justine Jacquard Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 6300,
                    "wasPrice": 9000,
                    "title": "Laura Denim Shirt Dress",
                    "link": "http://www.boden.co.uk/en-gb/womens-dresses/day-dresses/ww140/womens-laura-denim-shirt-dress",
                    "rating": {
                        "value": 4.7,
                        "count": 39
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_ww140_lbl_w01.jpg",
                            "alt": "Laura Denim Shirt Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_ww140_lbl.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_ww140_lbl.jpg",
                            "alt": "Laura Denim Shirt Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_ww140_blu.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_ww140_blu.jpg",
                            "alt": "Laura Denim Shirt Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 7000,
                    "wasPrice": false,
                    "title": "Corinne Denim Dress",
                    "link": "http://www.boden.co.uk/en-gb/womens-dresses/day-dresses/w0025/womens-corinne-denim-dress",
                    "rating": {
                        "value": 3.9,
                        "count": 9
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0025_dbl_w01.jpg",
                            "alt": "Corinne Denim Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0025_blk.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0025_blk.jpg",
                            "alt": "Corinne Denim Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0025_blu.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0025_blu.jpg",
                            "alt": "Corinne Denim Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0025_dbl.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0025_dbl.jpg",
                            "alt": "Corinne Denim Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 8000,
                    "wasPrice": false,
                    "title": "Gloria Ponte Dress",
                    "link": "http://www.boden.co.uk/en-gb/womens-dresses/day-dresses/j0088/womens-gloria-ponte-dress",
                    "rating": {
                        "value": 5,
                        "count": 1
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0088_drd_w01.jpg",
                            "alt": "Gloria Ponte Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0088_blk.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0088_blk.jpg",
                            "alt": "Gloria Ponte Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0088_drd.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wwin_j0088_drd.jpg",
                            "alt": "Gloria Ponte Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 15000,
                    "wasPrice": false,
                    "title": "Edith Trim Detail Dress",
                    "link": "http://www.boden.co.uk/en-gb/womens-dresses/smart-day-dresses/w0061/womens-edith-trim-detail-dress",
                    "rating": {
                        "value": null,
                        "count": null
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0061_nav_w01.jpg",
                            "alt": "Edith Trim Detail Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0061_nav.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0061_nav.jpg",
                            "alt": "Edith Trim Detail Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 4500,
                    "wasPrice": 9000,
                    "title": "Jenna Shirt Dress",
                    "link": "http://www.boden.co.uk/en-gb/womens-dresses/day-dresses/w0018/womens-jenna-shirt-dress",
                    "rating": {
                        "value": 2.5,
                        "count": 2
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0018_nav_w02.jpg",
                            "alt": "Jenna Shirt Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_w0018_grn.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_w0018_grn.jpg",
                            "alt": "Jenna Shirt Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_w0018_nav.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_w0018_nav.jpg",
                            "alt": "Jenna Shirt Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 18000,
                    "wasPrice": false,
                    "title": "Robyn Velvet Dress",
                    "link": "http://www.boden.co.uk/en-gb/womens-dresses/party-dresses/w0056/womens-robyn-velvet-dress",
                    "rating": {
                        "value": null,
                        "count": null
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0056_prp_m01.jpg",
                            "alt": "Robyn Velvet Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0056_blk.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0056_blk.jpg",
                            "alt": "Robyn Velvet Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0056_prp.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wwin_w0056_prp.jpg",
                            "alt": "Robyn Velvet Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 11900,
                    "wasPrice": 17000,
                    "title": "Victoria British Tweed Blazer",
                    "link": "http://www.boden.co.uk/en-gb/womens-coats-jackets/blazers/t0086/womens-victoria-british-tweed-blazer",
                    "rating": {
                        "value": 4.8,
                        "count": 4
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_t0086_nav_w02.jpg",
                            "alt": "Victoria British Tweed Blazer"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_t0086_nav.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_t0086_nav.jpg",
                            "alt": "Victoria British Tweed Blazer"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 9800,
                    "wasPrice": false,
                    "title": "Nell Ponte Blazer",
                    "link": "http://www.boden.co.uk/en-gb/womens-coats-jackets/blazers/t0036/womens-nell-ponte-blazer",
                    "rating": {
                        "value": 3.6,
                        "count": 17
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wwin_t0036_blk_w01.jpg",
                            "alt": "Nell Ponte Blazer"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_t0036_blk.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_t0036_blk.jpg",
                            "alt": "Nell Ponte Blazer"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_t0036_mpr.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_t0036_mpr.jpg",
                            "alt": "Nell Ponte Blazer"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_t0036_nav.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_t0036_nav.jpg",
                            "alt": "Nell Ponte Blazer"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 2940,
                    "wasPrice": 9800,
                    "title": "Josephine Dress",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/day-dresses/ww286/womens-josephine-dress",
                    "rating": {
                        "value": 3.9,
                        "count": 57
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww286_mul_w01.jpg",
                            "alt": "Josephine Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww286_blu.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww286_blu.jpg",
                            "alt": "Josephine Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww286_mul.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww286_mul.jpg",
                            "alt": "Josephine Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww286_nav.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww286_nav.jpg",
                            "alt": "Josephine Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 4200,
                    "wasPrice": 7000,
                    "title": "Isabelle Dress",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/day-dresses/ww188/womens-isabelle-dress",
                    "rating": {
                        "value": 4,
                        "count": 19
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww188_nav_w01.jpg",
                            "alt": "Isabelle Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww188_mbl.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww188_mbl.jpg",
                            "alt": "Isabelle Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww188_nav.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww188_nav.jpg",
                            "alt": "Isabelle Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 3600,
                    "wasPrice": 6000,
                    "title": "Phoebe Jersey Dress",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/day-dresses/ww202/womens-phoebe-jersey-dress",
                    "rating": {
                        "value": 4.4,
                        "count": 284
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww202_bbl_w01.jpg",
                            "alt": "Phoebe Jersey Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww202_spt.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww202_spt.jpg",
                            "alt": "Phoebe Jersey Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww202_dpk.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww202_dpk.jpg",
                            "alt": "Phoebe Jersey Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww202_grn.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww202_grn.jpg",
                            "alt": "Phoebe Jersey Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww202_bbl.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww202_bbl.jpg",
                            "alt": "Phoebe Jersey Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww202_dbl.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww202_dbl.jpg",
                            "alt": "Phoebe Jersey Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww202_nvy.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww202_nvy.jpg",
                            "alt": "Phoebe Jersey Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww202_blu.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww202_blu.jpg",
                            "alt": "Phoebe Jersey Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww202_red.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww202_red.jpg",
                            "alt": "Phoebe Jersey Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 7200,
                    "wasPrice": 9000,
                    "title": "Posy Shirt Dress",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/day-dresses/ww193/womens-posy-shirt-dress",
                    "rating": {
                        "value": 4.4,
                        "count": 57
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_ww193_blu_w01.jpg",
                            "alt": "Posy Shirt Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww193_nav.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww193_nav.jpg",
                            "alt": "Posy Shirt Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_ww193_dbl.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_ww193_dbl.jpg",
                            "alt": "Posy Shirt Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww193_nvy.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww193_nvy.jpg",
                            "alt": "Posy Shirt Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww193_pnk.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww193_pnk.jpg",
                            "alt": "Posy Shirt Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17waut_ww193_blu.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17waut_ww193_blu.jpg",
                            "alt": "Posy Shirt Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 2700,
                    "wasPrice": 9000,
                    "title": "Lois Dress",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/day-dresses/ww219/womens-lois-dress",
                    "rating": {
                        "value": 3.5,
                        "count": 87
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww219_blu_w01.jpg",
                            "alt": "Lois Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww219_dpk.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww219_dpk.jpg",
                            "alt": "Lois Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww219_chm.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww219_chm.jpg",
                            "alt": "Lois Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww219_ivo.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww219_ivo.jpg",
                            "alt": "Lois Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww219_yel.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww219_yel.jpg",
                            "alt": "Lois Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww219_blu.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww219_blu.jpg",
                            "alt": "Lois Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 3200,
                    "wasPrice": 8000,
                    "title": "Lara Wrap Dress",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/day-dresses/ww213/womens-lara-wrap-dress",
                    "rating": {
                        "value": 3.7,
                        "count": 140
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww213_ivo_w01.jpg",
                            "alt": "Lara Wrap Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww213_chm.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww213_chm.jpg",
                            "alt": "Lara Wrap Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww213_grn.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww213_grn.jpg",
                            "alt": "Lara Wrap Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww213_nvy.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww213_nvy.jpg",
                            "alt": "Lara Wrap Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww213_nav.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww213_nav.jpg",
                            "alt": "Lara Wrap Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww213_red.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww213_red.jpg",
                            "alt": "Lara Wrap Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 4800,
                    "wasPrice": 8000,
                    "title": "Gloria Ponte Dress",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/day-dresses/ww198/womens-gloria-ponte-dress",
                    "rating": {
                        "value": 3.8,
                        "count": 21
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww198_nav_w01.jpg",
                            "alt": "Gloria Ponte Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww198_nav.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww198_nav.jpg",
                            "alt": "Gloria Ponte Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 7200,
                    "wasPrice": 9000,
                    "title": "Talia Dress",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/day-dresses/ww244/womens-talia-dress",
                    "rating": {
                        "value": 4.2,
                        "count": 41
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww244_ivo_w01.jpg",
                            "alt": "Talia Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww244_dbl.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww244_dbl.jpg",
                            "alt": "Talia Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww244_yel.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww244_yel.jpg",
                            "alt": "Talia Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww244_ivo.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww244_ivo.jpg",
                            "alt": "Talia Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 3600,
                    "wasPrice": 9000,
                    "title": "Zoe Dress",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/day-dresses/ww256/womens-zoe-dress",
                    "rating": {
                        "value": 3.8,
                        "count": 18
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww256_grn_w01.jpg",
                            "alt": "Zoe Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww256_dpk.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww256_dpk.jpg",
                            "alt": "Zoe Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww256_grn.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww256_grn.jpg",
                            "alt": "Zoe Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww256_nav.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww256_nav.jpg",
                            "alt": "Zoe Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 6300,
                    "wasPrice": 9000,
                    "title": "Riva Jacquard Dress",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/smart-day-dresses/ww186/womens-riva-jacquard-dress",
                    "rating": {
                        "value": 4.5,
                        "count": 22
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww186_yel_w02.jpg",
                            "alt": "Riva Jacquard Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww186_red.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww186_red.jpg",
                            "alt": "Riva Jacquard Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 7200,
                    "wasPrice": 12000,
                    "title": "Romilly Dress",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/smart-day-dresses/ww229/womens-romilly-dress",
                    "rating": {
                        "value": 3.2,
                        "count": 18
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww229_bgr_w01.jpg",
                            "alt": "Romilly Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww229_bgr.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww229_bgr.jpg",
                            "alt": "Romilly Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww229_grn.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww229_grn.jpg",
                            "alt": "Romilly Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww229_pnk.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww229_pnk.jpg",
                            "alt": "Romilly Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 6500,
                    "wasPrice": 13000,
                    "title": "Patricia Dress",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/smart-day-dresses/ww253/womens-patricia-dress",
                    "rating": {
                        "value": 3.6,
                        "count": 8
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww253_blu_w01.jpg",
                            "alt": "Patricia Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww253_blu.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww253_blu.jpg",
                            "alt": "Patricia Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 4800,
                    "wasPrice": 12000,
                    "title": "Ellen Cotton Blazer",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-coats-jackets/jackets/we588/womens-ellen-cotton-blazer",
                    "rating": {
                        "value": 4.3,
                        "count": 44
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_we588_spt_w01.jpg",
                            "alt": "Ellen Cotton Blazer"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_we588_yel.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_we588_yel.jpg",
                            "alt": "Ellen Cotton Blazer"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_we588_nav.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_we588_nav.jpg",
                            "alt": "Ellen Cotton Blazer"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 3600,
                    "wasPrice": 6000,
                    "title": "Rosa Dress",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/day-dresses/ww218/womens-rosa-dress",
                    "rating": {
                        "value": 4.3,
                        "count": 56
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww218_wht_w01.jpg",
                            "alt": "Rosa Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww218_blu.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww218_blu.jpg",
                            "alt": "Rosa Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww218_pnk.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww218_pnk.jpg",
                            "alt": "Rosa Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww218_grn.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww218_grn.jpg",
                            "alt": "Rosa Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww218_wht.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww218_wht.jpg",
                            "alt": "Rosa Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 5600,
                    "wasPrice": 14000,
                    "title": "Vanda Dress",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/day-dresses/ww292/womens-vanda-dress",
                    "rating": {
                        "value": 4.4,
                        "count": 30
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww292_nav_w01.jpg",
                            "alt": "Vanda Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww292_blu.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww292_blu.jpg",
                            "alt": "Vanda Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww292_nav.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww292_nav.jpg",
                            "alt": "Vanda Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww292_red.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww292_red.jpg",
                            "alt": "Vanda Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 4200,
                    "wasPrice": 6000,
                    "title": "Sorrento Jersey Shift Dress",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/day-dresses/ww205/womens-sorrento-jersey-shift-dress",
                    "rating": {
                        "value": 4.5,
                        "count": 39
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wspr_ww205_ivo.jpg",
                            "alt": "Sorrento Jersey Shift Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 4800,
                    "wasPrice": 8000,
                    "title": "Henrietta Linen Dress",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/day-dresses/ww259/womens-henrietta-linen-dress",
                    "rating": {
                        "value": 4.3,
                        "count": 25
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww259_wht_w01.jpg",
                            "alt": "Henrietta Linen Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww259_dbl.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww259_dbl.jpg",
                            "alt": "Henrietta Linen Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww259_wht.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww259_wht.jpg",
                            "alt": "Henrietta Linen Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 5600,
                    "wasPrice": 8000,
                    "title": "Sophia Shirt Dress",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/day-dresses/ww257/womens-sophia-shirt-dress",
                    "rating": {
                        "value": 4.4,
                        "count": 39
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww257_yel_w01.jpg",
                            "alt": "Sophia Shirt Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww257_dpk.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww257_dpk.jpg",
                            "alt": "Sophia Shirt Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww257_chm.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww257_chm.jpg",
                            "alt": "Sophia Shirt Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww257_grn.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww257_grn.jpg",
                            "alt": "Sophia Shirt Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww257_yel.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww257_yel.jpg",
                            "alt": "Sophia Shirt Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww257_wht.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww257_wht.jpg",
                            "alt": "Sophia Shirt Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 5880,
                    "wasPrice": 9800,
                    "title": "Annecy Full Skirt Maxi Dress",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/maxi-dresses/ww278/womens-annecy-full-skirt-maxi-dress",
                    "rating": {
                        "value": 4.5,
                        "count": 44
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww278_nvy_w01.jpg",
                            "alt": "Annecy Full Skirt Maxi Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww278_blk.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww278_blk.jpg",
                            "alt": "Annecy Full Skirt Maxi Dress"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww278_nvy.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_ww278_nvy.jpg",
                            "alt": "Annecy Full Skirt Maxi Dress"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 2250,
                    "wasPrice": 4500,
                    "title": "Adriana Tunic",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/day-dresses/wo163/womens-adriana-tunic",
                    "rating": {
                        "value": 3.9,
                        "count": 15
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_wo163_mul_w01.jpg",
                            "alt": "Adriana Tunic"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_wo163_lbl.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_wo163_lbl.jpg",
                            "alt": "Adriana Tunic"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_wo163_mul.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_wo163_mul.jpg",
                            "alt": "Adriana Tunic"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_wo163_stp.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_wo163_stp.jpg",
                            "alt": "Adriana Tunic"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_wo163_nav.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_wo163_nav.jpg",
                            "alt": "Adriana Tunic"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_wo163_aqu.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_wo163_aqu.jpg",
                            "alt": "Adriana Tunic"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_wo163_pnk.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_wo163_pnk.jpg",
                            "alt": "Adriana Tunic"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 3200,
                    "wasPrice": 8000,
                    "title": "Jersey Jumpsuit",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-dresses/day-dresses/wm473/womens-jersey-jumpsuit",
                    "rating": {
                        "value": 4.5,
                        "count": 19
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_wm473_khk_w01.jpg",
                            "alt": "Jersey Jumpsuit"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_wm473_blu.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_wm473_blu.jpg",
                            "alt": "Jersey Jumpsuit"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_wm473_khk.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_wm473_khk.jpg",
                            "alt": "Jersey Jumpsuit"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 5200,
                    "wasPrice": 13000,
                    "title": "Broderie Blazer",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-coats-jackets/jackets/we604/womens-broderie-blazer",
                    "rating": {
                        "value": 3.3,
                        "count": 3
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_we604_pnk.jpg",
                            "alt": "Broderie Blazer"
                        },
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/17wsum_we604_wht.jpg",
                            "thumb": "http://www.bodenimages.com/productimages/productmedium/17wsum_we604_wht.jpg",
                            "alt": "Broderie Blazer"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boden",
                    "price": 3475,
                    "wasPrice": 6950,
                    "title": "Maude Skirt",
                    "link": "http://www.boden.co.uk/en-gb/clearance/womens-skirts/below-knee-skirts/wg688/womens-maude-skirt",
                    "rating": {
                        "value": 4.3,
                        "count": 32
                    },
                    "images": [
                        {
                            "src": "http://www.bodenimages.com/productimages/productmedium/16wwin_wg688_nav.jpg",
                            "alt": "Maude Skirt"
                        }
                    ],
                    "categories": [
                        "dresses"
                    ]
                }
            ]);
        });

    });

    it("throws an error if the initial read errors", () => {
        const error = {
            isError: true
        };
        mockRequest.mock.setResponse("get", error, null, null);

        return getBoden()
            .then(failTest)
            .catch((returnedError) => {
                expect(returnedError).to.equal(error);
            });

    });

    it("throws an error if the read produces no valid products", () => {
        const mockPageSnippet = `<div id="search-result-items"></div>`;

        mockRequest.mock.setResponse("get", null, null, mockPageSnippet);

        return getBoden()
            .then(failTest)
            .catch((returnedError) => {
                expect(returnedError).to.deep.equal("No valid products were found");
            });
    });
});