module.exports = {
    isValidProduct: (product) => {
        if (
            product &&
            product.vendor &&
            product.price &&
            product.title &&
            product.link &&
            product.images &&
            product.images.length &&
            product.categories &&
            product.categories.length
        ) {
            return true;
        }

        return false;
    }
};