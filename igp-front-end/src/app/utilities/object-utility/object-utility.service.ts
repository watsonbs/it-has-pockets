import { Injectable } from '@angular/core';

@Injectable()
export class ObjectUtilityService {
  isObject(item) {
    return item === Object(item);
  }
  recursiveMerge(...objects) {
    if (!objects.length) return {};
    if (objects.length === 1) return objects[0];

    return objects.reduce((mergeResult, object) => {
      const keys = Object.keys(object);

      keys.forEach((key) => {
        if (mergeResult[key]) {
          if (Array.isArray(mergeResult[key]) && Array.isArray(object[key])) {
            mergeResult[key] = mergeResult[key].concat(object[key]);
            return;
          }

          if (this.isObject(mergeResult[key]) && this.isObject(object[key])) {
            mergeResult[key] = this.recursiveMerge(mergeResult[key], object[key]);
            return;
          }
        }
        mergeResult[key] = object[key];
      });

      return mergeResult;
    }, {});
  }
  constructor() {
  }

}
