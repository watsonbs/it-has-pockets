import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ObjectUtilityService } from './object-utility.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [ObjectUtilityService]
})
export class ObjectUtilityModule { }
