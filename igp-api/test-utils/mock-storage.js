const sinon = require("sinon");

const mockStorage = {
    put: sinon.stub(),
    mock: {
        setResponse: (method, data) => {
            mockStorage[method].returns(Promise.resolve(data));
        },
        reset: () => {
            mockStorage.put.reset();
        }
    }
};

module.exports = mockStorage;