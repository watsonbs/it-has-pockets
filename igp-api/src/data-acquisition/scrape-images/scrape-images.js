const md5 = require("md5");
const request = require("request");
const storage = require("../../utilities/storage");
const logger = require("../../utilities/logger");
const resizeImg = require('resize-img');

const removeMissingImagesFromProduct = (product) => {
    product.images = product.images.filter(image => image);
    return product;
};
const removeProductsWithoutImages = (productList) => productList.filter(product => (product.images && product.images.length));
const removeImagesWithoutSrcs = (productList) => {
    productList.forEach((product) => {
        if (product.images) {
            product.images.filter(image => Boolean(image.src));
        }
    });

    return productList;
}

const resizeImage = (image, dimensions) => {
    if (dimensions) {
        return resizeImg(body, dimensions);
    } else {
        return Promise.resolve(image);
    }
};
const scrapeImage = (imageUrl, product, index, propertyName, dimensions) => {
    const imageKey = md5(imageUrl);

    return storage.getBlob(imageKey)
        .catch((error) => {
            if (error.code !== "NoSuchKey") throw error;

            return new Promise((resolve, reject) => {
                request.get(imageUrl, (error, response, body) => {
                    if (error) {
                        reject(error);
                    }

                    resizeImage(body, dimensions)
                        .then((image) => {
                            resolve(storage.put(imageKey, image));
                        });
                });
            });
        })
        .then(() => {
            product.images[index][propertyName] = imageKey;
        })
        .catch(error => {
            logger.error(error);
            product.images[index] = false;
            return Promise.resolve();
        });
};
const scrapeProductImages = (product) => {
    let imageScrapes = [];
    product.images.forEach((imageObject, index) => {
        const imageUrl = imageObject.src;
        const imageThumb = imageObject.thumb;
        if (!imageUrl) product.images[index] = false;

        imageScrapes.push(scrapeImage(imageUrl, product, index, "src", {
            width: 249
        }));

        if (imageThumb) {
            imageScrapes.push(scrapeImage(imageThumb, product, index, "thumb", {
                width: 28
            }));
        } else {
            imageScrapes.push(scrapeImage(imageUrl, product, index, "thumb", {
                width: 28
            }));
        }
    });
    return Promise.all(imageScrapes);
};
const cloneProduct = (product) => {
    return JSON.parse(JSON.stringify(product))
};
const scrapeProductListImages = (productList) => {
    let productScrapes = [];
    productList.forEach((product) => {
        const productClone = cloneProduct(product);
        productScrapes.push(
            scrapeProductImages(productClone)
            .then(() => removeMissingImagesFromProduct(productClone))
        );
    });
    return productScrapes;
};

module.exports = (productList) => {
    return Promise.all(scrapeProductListImages(productList))
        .then(removeImagesWithoutSrcs)
        .then(removeProductsWithoutImages);
};