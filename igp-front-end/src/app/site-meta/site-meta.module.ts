import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutSiteComponent } from './about-site/about-site.component';
import { ContactSiteComponent } from './contact-site/contact-site.component';
import { routing } from './site-meta.routing';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    routing
  ],
  declarations: [AboutSiteComponent, ContactSiteComponent]
})
export class SiteMetaModule { }
