const chai = require('chai')
    .use(require('sinon-chai'));

const sinon = require('sinon');
const expect = chai.expect;
const fs = require("fs");
const proxyquire = require("proxyquire");
const mockRequest = require("../../../../test-utils/mock-request");
const cheerio = require("cheerio");
const mockFullBodenPage = fs.readFileSync("./src/data-acquisition/product-scrapers/get-boohoo/mocks/mock-body.html");

const getBoohoo = proxyquire('./get-boohoo', {
    request: mockRequest
});

const failTest = () => {
    expect(true).to.equal(false, "Test reached an invalid state. A promise probably threw an unexpected error");
}

describe("getBoohoo", () => {
    afterEach(() => {
        mockRequest.mock.reset();
    });

    it("does not include items which fail validation", () => {
        const mockPageSnippet = `
            <div id="search-result-items">
                <div class="grid-tile">
                    <div class="product-tile" id="f810b7f68e310635c42b2d3414" data-itemid="DZZ89277" data-product-tile="{&quot;id&quot;:&quot;DZZ89277&quot;,&quot;name&quot;:&quot;Lois Slouch Pocket Denim Dress&quot;,&quot;category&quot;:&quot;Dresses&quot;,&quot;dimension60&quot;:&quot;Casual&quot;,&quot;dimension62&quot;:&quot;Shift Dresses&quot;,&quot;brand&quot;:&quot;Boohoo Blue&quot;,&quot;dimension61&quot;:&quot;Female&quot;,&quot;metric36&quot;:87,&quot;metric37&quot;:86,&quot;dimension66&quot;:&quot;plp&quot;,&quot;price&quot;:&quot;25.00&quot;,&quot;salePrice&quot;:null,&quot;promoPrice&quot;:null,&quot;dimension65&quot;:&quot;mid blue&quot;,&quot;dimension64&quot;:&quot;Colour&quot;,&quot;dimension68&quot;:&quot;10&quot;}">
                        <div class="product-image">
                            <a class="thumb-link" href="/lois-slouch-pocket-denim-dress/DZZ89277.html" title="Lois Slouch Pocket Denim Dress">
                                <picture>
                                    <source class="js-ampliance-mobile-very-small" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_very_small_mobile$"
                                        media="(max-width: 379px)">
                                    <source class="js-ampliance-mobile-small" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_small_mobile$"
                                        media="(min-width: 380px) and (max-width: 459px)">
                                    <source class="js-ampliance-mobile-medium" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_medium_mobile$"
                                        media="(min-width: 460px) and (max-width: 559px)">
                                    <source class="js-ampliance-mobile-large" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_large_mobile$"
                                        media="(min-width: 560px) and (max-width: 629px)">
                                    <source class="js-ampliance-mobile" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_very_large_mobile$"
                                        media="(min-width: 630px) and (max-width: 767px)">
                                    <source class="js-ampliance-tablet-portrait" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_tablet_portrait$"
                                        media="(min-width: 768px) and (max-width: 1023px)">
                                    <source class="js-ampliance-tablet" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_tablet$"
                                        media="(min-width: 1024px) and (max-width: 1279px)">
                                    <source class="js-ampliance-desktop-small" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_small_desktop$"
                                        media="(min-width: 1280px) and (max-width: 1365px)">
                                    <source class="js-ampliance-landscape-pro" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_tablet_landscape_pro$"
                                        media="(min-width: 1366px) and (max-width: 1535px)">
                                    <source class="js-ampliance-desktop" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page$"
                                        media="(min-width: 1536px)">
                                    <img itemprop="image" alt="Lois Slouch Pocket Denim Dress" />
                                </picture>
                            </a>
                        </div>
                        <div class="product-name">
                        </div>
                        <div class="product-pricing">
                            <span class="product-sales-price" title="Sale Price">&pound;25.00</span>
                        </div>
                    </div>
                </div>
                <div class="grid-tile">
                    <div class="product-tile" id="f810b7f68e310635c42b2d3414" data-itemid="DZZ89277" data-product-tile="{&quot;id&quot;:&quot;DZZ89277&quot;,&quot;name&quot;:&quot;Lois Slouch Pocket Denim Dress&quot;,&quot;category&quot;:&quot;Dresses&quot;,&quot;dimension60&quot;:&quot;Casual&quot;,&quot;dimension62&quot;:&quot;Shift Dresses&quot;,&quot;brand&quot;:&quot;Boohoo Blue&quot;,&quot;dimension61&quot;:&quot;Female&quot;,&quot;metric36&quot;:87,&quot;metric37&quot;:86,&quot;dimension66&quot;:&quot;plp&quot;,&quot;price&quot;:&quot;25.00&quot;,&quot;salePrice&quot;:null,&quot;promoPrice&quot;:null,&quot;dimension65&quot;:&quot;mid blue&quot;,&quot;dimension64&quot;:&quot;Colour&quot;,&quot;dimension68&quot;:&quot;10&quot;}">
                        <div class="product-image">
                            <a class="thumb-link" href="/lois-slouch-pocket-denim-dress/DZZ89277.html" title="Lois Slouch Pocket Denim Dress">
                                <picture>
                                    <source class="js-ampliance-mobile-very-small" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_very_small_mobile$"
                                        media="(max-width: 379px)">
                                    <source class="js-ampliance-mobile-small" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_small_mobile$"
                                        media="(min-width: 380px) and (max-width: 459px)">
                                    <source class="js-ampliance-mobile-medium" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_medium_mobile$"
                                        media="(min-width: 460px) and (max-width: 559px)">
                                    <source class="js-ampliance-mobile-large" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_large_mobile$"
                                        media="(min-width: 560px) and (max-width: 629px)">
                                    <source class="js-ampliance-mobile" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_very_large_mobile$"
                                        media="(min-width: 630px) and (max-width: 767px)">
                                    <source class="js-ampliance-tablet-portrait" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_tablet_portrait$"
                                        media="(min-width: 768px) and (max-width: 1023px)">
                                    <source class="js-ampliance-tablet" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_tablet$"
                                        media="(min-width: 1024px) and (max-width: 1279px)">
                                    <source class="js-ampliance-desktop-small" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_small_desktop$"
                                        media="(min-width: 1280px) and (max-width: 1365px)">
                                    <source class="js-ampliance-landscape-pro" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_tablet_landscape_pro$"
                                        media="(min-width: 1366px) and (max-width: 1535px)">
                                    <source class="js-ampliance-desktop" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page$"
                                        media="(min-width: 1536px)">
                                    <img itemprop="image" alt="Lois Slouch Pocket Denim Dress" />
                                </picture>
                            </a>
                        </div>
                        <div class="product-name">
                            <a class="name-link" href="http://www.boohoo.com/lois-slouch-pocket-denim-dress/DZZ89277.html" title="Go to Product: Lois Slouch Pocket Denim Dress">
                                Lois Slouch Pocket Denim Dress
                            </a>
                        </div>
                        <div class="product-pricing">
                            <span class="product-sales-price" title="Sale Price">&pound;25.00</span>
                        </div>
                    </div>
                </div>
            </div>`;

        mockRequest.mock.setResponse("get", null, null, mockPageSnippet);

        return getBoohoo().then((boohooData) => {
            expect(boohooData).to.deep.equal([{
                vendor: 'boohoo',
                price: 2500,
                wasPrice: false,
                title: 'Lois Slouch Pocket Denim Dress',
                link: 'http://www.boohoo.com/lois-slouch-pocket-denim-dress/DZZ89277.html',
                rating: false,
                images: [ 
                    { 
                        src: '//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page$',
                        alt: 'Lois Slouch Pocket Denim Dress' 
                    } 
                ],
                descriptionHtml: false,
                categories: ['dresses']
            }]);
        });
    });

    it("requests the boohoo dresses with pockets search query page", () => {
        sinon.spy(cheerio, "load")
        const mockPageSnippet = `
            <div id="search-result-items">
                <div class="grid-tile">
                    <div class="product-tile" id="f810b7f68e310635c42b2d3414" data-itemid="DZZ89277" data-product-tile="{&quot;id&quot;:&quot;DZZ89277&quot;,&quot;name&quot;:&quot;Lois Slouch Pocket Denim Dress&quot;,&quot;category&quot;:&quot;Dresses&quot;,&quot;dimension60&quot;:&quot;Casual&quot;,&quot;dimension62&quot;:&quot;Shift Dresses&quot;,&quot;brand&quot;:&quot;Boohoo Blue&quot;,&quot;dimension61&quot;:&quot;Female&quot;,&quot;metric36&quot;:87,&quot;metric37&quot;:86,&quot;dimension66&quot;:&quot;plp&quot;,&quot;price&quot;:&quot;25.00&quot;,&quot;salePrice&quot;:null,&quot;promoPrice&quot;:null,&quot;dimension65&quot;:&quot;mid blue&quot;,&quot;dimension64&quot;:&quot;Colour&quot;,&quot;dimension68&quot;:&quot;10&quot;}">
                        <div class="product-image">
                            <a class="thumb-link" href="/lois-slouch-pocket-denim-dress/DZZ89277.html" title="Lois Slouch Pocket Denim Dress">
                                <picture>
                                    <source class="js-ampliance-mobile-very-small" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_very_small_mobile$"
                                        media="(max-width: 379px)">
                                    <source class="js-ampliance-mobile-small" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_small_mobile$"
                                        media="(min-width: 380px) and (max-width: 459px)">
                                    <source class="js-ampliance-mobile-medium" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_medium_mobile$"
                                        media="(min-width: 460px) and (max-width: 559px)">
                                    <source class="js-ampliance-mobile-large" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_large_mobile$"
                                        media="(min-width: 560px) and (max-width: 629px)">
                                    <source class="js-ampliance-mobile" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_very_large_mobile$"
                                        media="(min-width: 630px) and (max-width: 767px)">
                                    <source class="js-ampliance-tablet-portrait" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_tablet_portrait$"
                                        media="(min-width: 768px) and (max-width: 1023px)">
                                    <source class="js-ampliance-tablet" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_tablet$"
                                        media="(min-width: 1024px) and (max-width: 1279px)">
                                    <source class="js-ampliance-desktop-small" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_small_desktop$"
                                        media="(min-width: 1280px) and (max-width: 1365px)">
                                    <source class="js-ampliance-landscape-pro" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_tablet_landscape_pro$"
                                        media="(min-width: 1366px) and (max-width: 1535px)">
                                    <source class="js-ampliance-desktop" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page$"
                                        media="(min-width: 1536px)">
                                    <img itemprop="image" alt="Lois Slouch Pocket Denim Dress" />
                                </picture>
                            </a>
                        </div>
                        <div class="product-name">
                            <a class="name-link" href="http://www.boohoo.com/lois-slouch-pocket-denim-dress/DZZ89277.html" title="Go to Product: Lois Slouch Pocket Denim Dress">
                                Lois Slouch Pocket Denim Dress
                            </a>
                        </div>
                        <div class="product-pricing">
                            <span class="product-sales-price" title="Sale Price">&pound;25.00</span>
                        </div>
                    </div>
                </div>
            </div>`;
        const dressesWithPocketsUrl = "http://www.boohoo.com/search?q=pockets&prefn1=category&prefn2=gender&prefv1=Dresses&prefv2=Female";
        mockRequest.mock.setResponse("get", null, null, mockPageSnippet);

        return getBoohoo().then(() => {
            expect(mockRequest.get).calledWith(dressesWithPocketsUrl);
            expect(cheerio.load).calledWith(mockPageSnippet);
            cheerio.load.restore();
        });

    });

    it("Uses the main image if there are no alternate images", () => {
        const boohooFragment = `<div id="search-result-items">
            <div class="product-tile" id="f810b7f68e310635c42b2d3414" data-itemid="DZZ89277" data-product-tile="{&quot;id&quot;:&quot;DZZ89277&quot;,&quot;name&quot;:&quot;Lois Slouch Pocket Denim Dress&quot;,&quot;category&quot;:&quot;Dresses&quot;,&quot;dimension60&quot;:&quot;Casual&quot;,&quot;dimension62&quot;:&quot;Shift Dresses&quot;,&quot;brand&quot;:&quot;Boohoo Blue&quot;,&quot;dimension61&quot;:&quot;Female&quot;,&quot;metric36&quot;:87,&quot;metric37&quot;:86,&quot;dimension66&quot;:&quot;plp&quot;,&quot;price&quot;:&quot;25.00&quot;,&quot;salePrice&quot;:null,&quot;promoPrice&quot;:null,&quot;dimension65&quot;:&quot;mid blue&quot;,&quot;dimension64&quot;:&quot;Colour&quot;,&quot;dimension68&quot;:&quot;10&quot;}">
            <div class="product-image">
                <a class="thumb-link" href="/lois-slouch-pocket-denim-dress/DZZ89277.html" title="Lois Slouch Pocket Denim Dress">
                    <picture>
                        <source class="js-ampliance-mobile-very-small" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_very_small_mobile$"
                            media="(max-width: 379px)">
                        <source class="js-ampliance-mobile-small" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_small_mobile$"
                            media="(min-width: 380px) and (max-width: 459px)">
                        <source class="js-ampliance-mobile-medium" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_medium_mobile$"
                            media="(min-width: 460px) and (max-width: 559px)">
                        <source class="js-ampliance-mobile-large" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_large_mobile$"
                            media="(min-width: 560px) and (max-width: 629px)">
                        <source class="js-ampliance-mobile" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_very_large_mobile$"
                            media="(min-width: 630px) and (max-width: 767px)">
                        <source class="js-ampliance-tablet-portrait" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_tablet_portrait$"
                            media="(min-width: 768px) and (max-width: 1023px)">
                        <source class="js-ampliance-tablet" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_tablet$"
                            media="(min-width: 1024px) and (max-width: 1279px)">
                        <source class="js-ampliance-desktop-small" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_small_desktop$"
                            media="(min-width: 1280px) and (max-width: 1365px)">
                        <source class="js-ampliance-landscape-pro" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page_tablet_landscape_pro$"
                            media="(min-width: 1366px) and (max-width: 1535px)">
                        <source class="js-ampliance-desktop" srcset="//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page$"
                            media="(min-width: 1536px)">
                        <img itemprop="image" alt="Lois Slouch Pocket Denim Dress" />
                    </picture>
                </a>
            </div>
            <div class="product-name">
                <a class="name-link" href="http://www.boohoo.com/lois-slouch-pocket-denim-dress/DZZ89277.html" title="Go to Product: Lois Slouch Pocket Denim Dress">
                    Lois Slouch Pocket Denim Dress
                </a>
            </div>
            <div class="product-pricing">
                <span class="product-sales-price" title="Sale Price">&pound;25.00</span>
            </div>
        </div>
        </div>`;
        mockRequest.mock.setResponse("get", null, null, boohooFragment);

        return getBoohoo().then((boohooData) => {
            expect(boohooData).to.deep.equal([ { vendor: 'boohoo',
            price: 2500,
            wasPrice: false,
            title: 'Lois Slouch Pocket Denim Dress',
            link: 'http://www.boohoo.com/lois-slouch-pocket-denim-dress/DZZ89277.html',
            rating: false,
            images: [ 
                {
                    "alt": "Lois Slouch Pocket Denim Dress",
                    "src": "//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page$"
                }
            ],
            descriptionHtml: false,
            categories: [ 'dresses' ] } ]);
        });
    });

    it("Uses all images in the swatch if they exist", () => {
        const boohooFragment = `<div id="search-result-items">
        <li class="grid-tile new-row" data-colors-to-show="148,107,208">

            <script type="text/javascript">
                //<!--
                /* <![CDATA[ */
                (function () {
                    try {
                        if (window.CQuotient) {
                            var cq_params = {};

                            cq_params.cookieId = window.CQuotient.getCQCookieId();
                            cq_params.userId = window.CQuotient.getCQUserId();
                            cq_params.accumulate = true;
                            cq_params.searchText = 'pockets';
                            if (true)
                                cq_params.suggestedSearchText = 'pockets';
                            cq_params.products = [{
                                id: 'DZZ50762',
                                sku: '',
                                textRelevance: '0.8072661',
                                sortDetails: '{\"sortValues\":[{\"attrScore\":8.0,\"relevanceWeighted\":false,\"rawScore\":8.0,\"dynamic\":false,\"attrName\":\"Search Placement (product.searchPlacement)\",\"personalized\":false},{\"dynamicAttrs\":[{\"min\":0.0,\"avg\":649.5993670886076,\"dynAttrName\":\"Revenue (7 Days) (activeData.revenueWeek)\",\"max\":4217.2,\"weight\":0.2,\"weightedValue\":0.2,\"normalizedValue\":1.0,\"dynAttrValue\":4217.2,\"direction\":\"descending\"},{\"min\":0.0,\"avg\":6.272151898734177,\"dynAttrName\":\"Units Ordered (1 Day) (activeData.unitsDay)\",\"max\":38.0,\"weight\":0.2,\"weightedValue\":0.2,\"normalizedValue\":1.0,\"dynAttrValue\":38.0,\"direction\":\"descending\"},{\"min\":0.047619047619047616,\"avg\":0.5548712753713035,\"dynAttrName\":\"SKU Coverage (availabilityModel.skuCoverage)\",\"max\":0.9649198475417989,\"weight\":0.3,\"weightedValue\":0.17448965880241576,\"normalizedValue\":0.5816321960080526,\"dynAttrValue\":0.5811507262780607,\"direction\":\"descending\"},{\"min\":2.0,\"avg\":222.45294117647057,\"dynAttrName\":\"ATS (availabilityModel.ats)\",\"max\":614.0,\"weight\":0.3,\"weightedValue\":0.09313725490196079,\"normalizedValue\":0.3104575163398693,\"dynAttrValue\":192.0,\"direction\":\"descending\"}],\"attrScore\":0.5389525856581845,\"relevanceWeighted\":true,\"rawScore\":0.6676269137043765,\"dynamic\":true,\"attrName\":\"units ordered default rule\",\"personalized\":false},{\"attrScore\":0.807266116142273,\"relevanceWeighted\":false,\"rawScore\":null,\"dynamic\":false,\"attrName\":\"Text Relevance (sorting.text-relevance)\",\"personalized\":false}]}'
                            }];
                            cq_params.showProducts = 'true';
                            cq_params.personalized = 'false';
                            cq_params.refinements =
                                '[{\"name\":\"category\",\"value\":\"Dresses\"},{\"name\":\"gender\",\"value\":\"Female\"}]';
                            cq_params.sortingRule = 'most relevent';

                            if (window.CQuotient.sendActivity)
                                window.CQuotient.sendActivity(CQuotient.clientId, 'viewSearch',
                                    cq_params);
                            else
                                window.CQuotient.activities.push({
                                    activityType: 'viewSearch',
                                    parameters: cq_params
                                });
                        }
                    } catch (err) {}
                })();
                /* ]]> */
                // -->
            </script>
            <script type="text/javascript">
                //<!--
                /* <![CDATA[ (viewProduct-active_data.js) */
                dw.ac.capture({
                    id: "DZZ50762",
                    type: "searchhit"
                });
                /* ]]> */
                // -->
            </script>
            <div class="product-tile" id="0b68b6b6d4b9415cca4feef776" data-itemid="DZZ50762" data-product-tile="{&quot;id&quot;:&quot;DZZ50762&quot;,&quot;name&quot;:&quot;Penny Pocket Front Cord Pinafore Dress&quot;,&quot;category&quot;:&quot;Dresses&quot;,&quot;dimension60&quot;:&quot;Casual&quot;,&quot;dimension62&quot;:&quot;Pinafore Dress&quot;,&quot;brand&quot;:&quot;Boohoo Blue&quot;,&quot;dimension61&quot;:&quot;Female&quot;,&quot;metric36&quot;:0,&quot;metric37&quot;:0,&quot;dimension66&quot;:&quot;plp&quot;,&quot;price&quot;:&quot;20.00&quot;,&quot;salePrice&quot;:null,&quot;promoPrice&quot;:null,&quot;dimension65&quot;:&quot;blush&quot;,&quot;dimension64&quot;:&quot;Colour&quot;,&quot;dimension68&quot;:&quot;10&quot;}">
                <div class="product-image">
                    <a class="thumb-link" href="/penny-pocket-front-cord-pinafore-dress/DZZ50762.html" title="Penny Pocket Front Cord Pinafore Dress">
                        <picture>
                            <source class="js-ampliance-mobile-very-small" srcset="//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page_very_small_mobile$"
                                media="(max-width: 379px)">
                            <source class="js-ampliance-mobile-small" srcset="//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page_small_mobile$"
                                media="(min-width: 380px) and (max-width: 459px)">
                            <source class="js-ampliance-mobile-medium" srcset="//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page_medium_mobile$"
                                media="(min-width: 460px) and (max-width: 559px)">
                            <source class="js-ampliance-mobile-large" srcset="//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page_large_mobile$"
                                media="(min-width: 560px) and (max-width: 629px)">
                            <source class="js-ampliance-mobile" srcset="//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page_very_large_mobile$"
                                media="(min-width: 630px) and (max-width: 767px)">
                            <source class="js-ampliance-tablet-portrait" srcset="//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page_tablet_portrait$"
                                media="(min-width: 768px) and (max-width: 1023px)">
                            <source class="js-ampliance-tablet" srcset="//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page_tablet$"
                                media="(min-width: 1024px) and (max-width: 1279px)">
                            <source class="js-ampliance-desktop-small" srcset="//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page_small_desktop$"
                                media="(min-width: 1280px) and (max-width: 1365px)">
                            <source class="js-ampliance-landscape-pro" srcset="//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page_tablet_landscape_pro$"
                                media="(min-width: 1366px) and (max-width: 1535px)">
                            <source class="js-ampliance-desktop" srcset="//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page$"
                                media="(min-width: 1536px)">
                            <img itemprop="image" alt="Penny Pocket Front Cord Pinafore Dress" />
                        </picture>
                    </a>
                </div>
                <div class="product-name">
                    <a class="name-link" href="http://www.boohoo.com/penny-pocket-front-cord-pinafore-dress/DZZ50762.html" title="Go to Product: Penny Pocket Front Cord Pinafore Dress">
                        Penny Pocket Front Cord Pinafore Dress
                    </a>
                </div>
                <div class="product-pricing">
                    <span class="product-sales-price" title="Sale Price">&pound;20.00</span>
                </div>
                <div class="product-swatches">
                    <ul class="swatch-list js-swatch-list">
                        <li class="product-swatch-item">
                            <a href="http://www.boohoo.com/penny-pocket-front-cord-pinafore-dress/DZZ50762.html?color=148" class="swatch" title="navy">
                                <img class="swatch-image" src="//i1.adis.ws/i/boohooamplience/dzz50762_navy_xl_s.jpg?$product_image_swatch$" alt="navy" data-thumb='{"src":"//i1.adis.ws/i/boohooamplience/dzz50762_navy_xl?$product_image_category_page$",
            "alt":"Penny Pocket Front Cord Pinafore Dress",
            "title":"Penny Pocket Front Cord Pinafore Dress",
            "srcMobileVerySmall":"//i1.adis.ws/i/boohooamplience/dzz50762_navy_xl?$product_image_category_page_very_small_mobile$",
            "srcMobileSmall":"//i1.adis.ws/i/boohooamplience/dzz50762_navy_xl?$product_image_category_page_small_mobile$",
            "srcMobileMedium":"//i1.adis.ws/i/boohooamplience/dzz50762_navy_xl?$product_image_category_page_medium_mobile$",
            "srcMobileLarge":"//i1.adis.ws/i/boohooamplience/dzz50762_navy_xl?$product_image_category_page_large_mobile$",
            "srcMobile":"//i1.adis.ws/i/boohooamplience/dzz50762_navy_xl?$product_image_category_page_very_large_mobile$",
            "srcTabletPortrait":"//i1.adis.ws/i/boohooamplience/dzz50762_navy_xl?$product_image_category_page_tablet_portrait$",
            "srcTablet":"//i1.adis.ws/i/boohooamplience/dzz50762_navy_xl?$product_image_category_page_tablet$",
            "srcTabletLandscapePro":"//i1.adis.ws/i/boohooamplience/dzz50762_navy_xl?$product_image_category_page_tablet_landscape_pro$",
            "srcDesktopSmall":"//i1.adis.ws/i/boohooamplience/dzz50762_navy_xl?$product_image_category_page_small_desktop$",
            "srcDesktop":"//i1.adis.ws/i/boohooamplience/dzz50762_navy_xl?$product_image_category_page$"}' />
                                        </a>
                                    </li>
                                    <li class="product-swatch-item">
                                        <a href="http://www.boohoo.com/penny-pocket-front-cord-pinafore-dress/DZZ50762.html?color=107" class="swatch" title="blush">
                                            <img class="swatch-image" src="//i1.adis.ws/i/boohooamplience/dzz50762_blush_xl_s.jpg?$product_image_swatch$" alt="blush"
                                                data-thumb='{"src":"//i1.adis.ws/i/boohooamplience/dzz50762_blush_xl?$product_image_category_page$",
            "alt":"Penny Pocket Front Cord Pinafore Dress",
            "title":"Penny Pocket Front Cord Pinafore Dress",
            "srcMobileVerySmall":"//i1.adis.ws/i/boohooamplience/dzz50762_blush_xl?$product_image_category_page_very_small_mobile$",
            "srcMobileSmall":"//i1.adis.ws/i/boohooamplience/dzz50762_blush_xl?$product_image_category_page_small_mobile$",
            "srcMobileMedium":"//i1.adis.ws/i/boohooamplience/dzz50762_blush_xl?$product_image_category_page_medium_mobile$",
            "srcMobileLarge":"//i1.adis.ws/i/boohooamplience/dzz50762_blush_xl?$product_image_category_page_large_mobile$",
            "srcMobile":"//i1.adis.ws/i/boohooamplience/dzz50762_blush_xl?$product_image_category_page_very_large_mobile$",
            "srcTabletPortrait":"//i1.adis.ws/i/boohooamplience/dzz50762_blush_xl?$product_image_category_page_tablet_portrait$",
            "srcTablet":"//i1.adis.ws/i/boohooamplience/dzz50762_blush_xl?$product_image_category_page_tablet$",
            "srcTabletLandscapePro":"//i1.adis.ws/i/boohooamplience/dzz50762_blush_xl?$product_image_category_page_tablet_landscape_pro$",
            "srcDesktopSmall":"//i1.adis.ws/i/boohooamplience/dzz50762_blush_xl?$product_image_category_page_small_desktop$",
            "srcDesktop":"//i1.adis.ws/i/boohooamplience/dzz50762_blush_xl?$product_image_category_page$"}' />
                                        </a>
                                    </li>
                                    <li class="product-swatch-item">
                                        <a href="http://www.boohoo.com/penny-pocket-front-cord-pinafore-dress/DZZ50762.html?color=208" class="swatch" title="rust">
                                            <img class="swatch-image" src="//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl_s.jpg?$product_image_swatch$" alt="rust" data-thumb='{"src":"//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page$",
            "alt":"Penny Pocket Front Cord Pinafore Dress",
            "title":"Penny Pocket Front Cord Pinafore Dress",
            "srcMobileVerySmall":"//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page_very_small_mobile$",
            "srcMobileSmall":"//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page_small_mobile$",
            "srcMobileMedium":"//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page_medium_mobile$",
            "srcMobileLarge":"//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page_large_mobile$",
            "srcMobile":"//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page_very_large_mobile$",
            "srcTabletPortrait":"//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page_tablet_portrait$",
            "srcTablet":"//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page_tablet$",
            "srcTabletLandscapePro":"//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page_tablet_landscape_pro$",
            "srcDesktopSmall":"//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page_small_desktop$",
            "srcDesktop":"//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page$"}' />
                                        </a>
                        </li>
                        <li class="show-all-colours">
                            <a href="http://www.boohoo.com/penny-pocket-front-cord-pinafore-dress/DZZ50762.html">
                                <span class="icon-plus"></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>   
        </div>`;
        mockRequest.mock.setResponse("get", null, null, boohooFragment);

        return getBoohoo().then((boohooData) => {
            expect(boohooData).to.deep.equal([{
                "vendor":"boohoo",
                "price":2000,
                "wasPrice":false,
                "title":"Penny Pocket Front Cord Pinafore Dress",
                "link":"http://www.boohoo.com/penny-pocket-front-cord-pinafore-dress/DZZ50762.html",
                "rating":false,
                "images":[
                    {
                        "thumb":"//i1.adis.ws/i/boohooamplience/dzz50762_navy_xl_s.jpg?$product_image_swatch$",
                        "src":"//i1.adis.ws/i/boohooamplience/dzz50762_navy_xl?$product_image_category_page$",
                        "alt":"navy",
                        "variantLink":"http://www.boohoo.com/penny-pocket-front-cord-pinafore-dress/DZZ50762.html?color=148"
                    },
                    {
                        "thumb":"//i1.adis.ws/i/boohooamplience/dzz50762_blush_xl_s.jpg?$product_image_swatch$",
                        "src":"//i1.adis.ws/i/boohooamplience/dzz50762_blush_xl?$product_image_category_page$",
                        "alt":"blush",
                        "variantLink":"http://www.boohoo.com/penny-pocket-front-cord-pinafore-dress/DZZ50762.html?color=107"
                    },
                    {
                        "thumb":"//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl_s.jpg?$product_image_swatch$",
                        "src":"//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page$",
                        "alt":"rust",
                        "variantLink":"http://www.boohoo.com/penny-pocket-front-cord-pinafore-dress/DZZ50762.html?color=208"
                    }
                ],
                "descriptionHtml":false,
                "categories":["dresses"]
            }]);
        });
    });

    it("parses the page into IGP product data", () => {
        mockRequest.mock.setResponse("get", null, null, mockFullBodenPage);

        return getBoohoo().then((boohooData) => {
            expect(boohooData.length).to.equal(34);
            expect(boohooData).to.deep.equal([
                {
                    "vendor": "boohoo",
                    "price": 2000,
                    "wasPrice": false,
                    "title": "Penny Pocket Front Cord Pinafore Dress",
                    "link": "http://www.boohoo.com/penny-pocket-front-cord-pinafore-dress/DZZ50762.html",
                    "rating": false,
                    "images": [
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz50762_navy_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz50762_navy_xl?$product_image_category_page$",
                            "alt": "navy",
                            "variantLink": "http://www.boohoo.com/penny-pocket-front-cord-pinafore-dress/DZZ50762.html?color=148"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz50762_blush_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz50762_blush_xl?$product_image_category_page$",
                            "alt": "blush",
                            "variantLink": "http://www.boohoo.com/penny-pocket-front-cord-pinafore-dress/DZZ50762.html?color=107"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz50762_rust_xl?$product_image_category_page$",
                            "alt": "rust",
                            "variantLink": "http://www.boohoo.com/penny-pocket-front-cord-pinafore-dress/DZZ50762.html?color=208"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 2500,
                    "wasPrice": false,
                    "title": "Lois Slouch Pocket Denim Dress",
                    "link": "http://www.boohoo.com/lois-slouch-pocket-denim-dress/DZZ89277.html",
                    "rating": false,
                    "images": [
                        {
                            "src": "//i1.adis.ws/i/boohooamplience/dzz89277_mid%2520blue_xl?$product_image_category_page$",
                            "alt": "Lois Slouch Pocket Denim Dress"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 2500,
                    "wasPrice": false,
                    "title": "Lorraine Long Sleeve Pocket Front Shirt Dress",
                    "link": "http://www.boohoo.com/lorraine-long-sleeve-pocket-front-shirt-dress/DZZ93023.html",
                    "rating": false,
                    "images": [
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz93023_dove_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz93023_dove_xl?$product_image_category_page$",
                            "alt": "dove",
                            "variantLink": "http://www.boohoo.com/lorraine-long-sleeve-pocket-front-shirt-dress/DZZ93023.html?color=431"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz93023_khaki_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz93023_khaki_xl?$product_image_category_page$",
                            "alt": "khaki",
                            "variantLink": "http://www.boohoo.com/lorraine-long-sleeve-pocket-front-shirt-dress/DZZ93023.html?color=135"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz93023_berry_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz93023_berry_xl?$product_image_category_page$",
                            "alt": "berry",
                            "variantLink": "http://www.boohoo.com/lorraine-long-sleeve-pocket-front-shirt-dress/DZZ93023.html?color=104"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz93023_black_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz93023_black_xl?$product_image_category_page$",
                            "alt": "black",
                            "variantLink": "http://www.boohoo.com/lorraine-long-sleeve-pocket-front-shirt-dress/DZZ93023.html?color=105"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 1500,
                    "wasPrice": false,
                    "title": "Flo Oversized Pocket Front Box Shift Dress",
                    "link": "http://www.boohoo.com/flo-oversized-pocket-front-box-shift-dress/DZZ69774.html",
                    "rating": false,
                    "images": [
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz69774_grey%20marl_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz69774_grey%20marl_xl?$product_image_category_page$",
                            "alt": "grey marl",
                            "variantLink": "http://www.boohoo.com/flo-oversized-pocket-front-box-shift-dress/DZZ69774.html?color=265"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz69774_khaki_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz69774_khaki_xl?$product_image_category_page$",
                            "alt": "khaki",
                            "variantLink": "http://www.boohoo.com/flo-oversized-pocket-front-box-shift-dress/DZZ69774.html?color=135"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz69774_berry_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz69774_berry_xl?$product_image_category_page$",
                            "alt": "berry",
                            "variantLink": "http://www.boohoo.com/flo-oversized-pocket-front-box-shift-dress/DZZ69774.html?color=104"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz69774_black_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz69774_black_xl?$product_image_category_page$",
                            "alt": "black",
                            "variantLink": "http://www.boohoo.com/flo-oversized-pocket-front-box-shift-dress/DZZ69774.html?color=105"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz69774_sand_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz69774_sand_xl?$product_image_category_page$",
                            "alt": "sand",
                            "variantLink": "http://www.boohoo.com/flo-oversized-pocket-front-box-shift-dress/DZZ69774.html?color=161"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 2500,
                    "wasPrice": false,
                    "title": "Kara Lace Up Front Pocket Detail Midi Dress",
                    "link": "http://www.boohoo.com/kara-lace-up-front-pocket-detail-midi-dress/DZZ37917.html?color=undefined",
                    "rating": false,
                    "images": [
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz37917_terracotta_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz37917_terracotta_xl?$product_image_category_page$",
                            "alt": "terracotta",
                            "variantLink": "http://www.boohoo.com/kara-lace-up-front-pocket-detail-midi-dress/DZZ37917.html?color=375"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz37917_khaki_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz37917_khaki_xl?$product_image_category_page$",
                            "alt": "khaki",
                            "variantLink": "http://www.boohoo.com/kara-lace-up-front-pocket-detail-midi-dress/DZZ37917.html?color=135"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz37917_black_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz37917_black_xl?$product_image_category_page$",
                            "alt": "black",
                            "variantLink": "http://www.boohoo.com/kara-lace-up-front-pocket-detail-midi-dress/DZZ37917.html?color=105"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 1200,
                    "wasPrice": false,
                    "title": "Alana Oversized Pocket Detail Shift Dress",
                    "link": "http://www.boohoo.com/alana-oversized-pocket-detail-shift-dress/DZZ41132.html",
                    "rating": false,
                    "images": [
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz41132_ivory_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz41132_ivory_xl?$product_image_category_page$",
                            "alt": "ivory",
                            "variantLink": "http://www.boohoo.com/alana-oversized-pocket-detail-shift-dress/DZZ41132.html?color=133"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz41132_black_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz41132_black_xl?$product_image_category_page$",
                            "alt": "black",
                            "variantLink": "http://www.boohoo.com/alana-oversized-pocket-detail-shift-dress/DZZ41132.html?color=105"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 2200,
                    "wasPrice": false,
                    "title": "Arianna Lace Up Utility Pocket Shirt Dress",
                    "link": "http://www.boohoo.com/arianna-lace-up-utility-pocket-shirt-dress/DZZ85178.html",
                    "rating": false,
                    "images": [
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz85178_black_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz85178_black_xl?$product_image_category_page$",
                            "alt": "black",
                            "variantLink": "http://www.boohoo.com/arianna-lace-up-utility-pocket-shirt-dress/DZZ85178.html?color=105"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz85178_cognac_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz85178_cognac_xl?$product_image_category_page$",
                            "alt": "cognac",
                            "variantLink": "http://www.boohoo.com/arianna-lace-up-utility-pocket-shirt-dress/DZZ85178.html?color=549"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz85178_olive_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz85178_olive_xl?$product_image_category_page$",
                            "alt": "olive",
                            "variantLink": "http://www.boohoo.com/arianna-lace-up-utility-pocket-shirt-dress/DZZ85178.html?color=151"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz85178_mocha_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz85178_mocha_xl?$product_image_category_page$",
                            "alt": "mocha",
                            "variantLink": "http://www.boohoo.com/arianna-lace-up-utility-pocket-shirt-dress/DZZ85178.html?color=197"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 1500,
                    "wasPrice": false,
                    "title": "Olivia Pocket Front Belted Midi Dress",
                    "link": "http://www.boohoo.com/olivia-pocket-front-belted-midi-dress/DZZ45475.html",
                    "rating": false,
                    "images": [
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz45475_stone_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz45475_stone_xl?$product_image_category_page$",
                            "alt": "stone",
                            "variantLink": "http://www.boohoo.com/olivia-pocket-front-belted-midi-dress/DZZ45475.html?color=165"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz45475_khaki_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz45475_khaki_xl?$product_image_category_page$",
                            "alt": "khaki",
                            "variantLink": "http://www.boohoo.com/olivia-pocket-front-belted-midi-dress/DZZ45475.html?color=135"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz45475_black_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz45475_black_xl?$product_image_category_page$",
                            "alt": "black",
                            "variantLink": "http://www.boohoo.com/olivia-pocket-front-belted-midi-dress/DZZ45475.html?color=105"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 2000,
                    "wasPrice": false,
                    "title": "Pansy Patch Pocket Denim Pinafore Dress",
                    "link": "http://www.boohoo.com/pansy-patch-pocket-denim-pinafore-dress/DZZ63135.html",
                    "rating": false,
                    "images": [
                        {
                            "src": "//i1.adis.ws/i/boohooamplience/dzz63135_mid%2520blue_xl?$product_image_category_page$",
                            "alt": "Pansy Patch Pocket Denim Pinafore Dress"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 1800,
                    "wasPrice": false,
                    "title": "Bianca Tailored Dress With Pocket",
                    "link": "http://www.boohoo.com/bianca-tailored-dress-with-pocket/DZZ54057.html",
                    "rating": false,
                    "images": [
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz54057_navy_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz54057_navy_xl?$product_image_category_page$",
                            "alt": "navy",
                            "variantLink": "http://www.boohoo.com/bianca-tailored-dress-with-pocket/DZZ54057.html?color=148"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz54057_black_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz54057_black_xl?$product_image_category_page$",
                            "alt": "black",
                            "variantLink": "http://www.boohoo.com/bianca-tailored-dress-with-pocket/DZZ54057.html?color=105"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz54057_grey_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz54057_grey_xl?$product_image_category_page$",
                            "alt": "grey",
                            "variantLink": "http://www.boohoo.com/bianca-tailored-dress-with-pocket/DZZ54057.html?color=131"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 2000,
                    "wasPrice": false,
                    "title": "Maeve Pocket Detail Sleeveless Shirt Dress",
                    "link": "http://www.boohoo.com/maeve-pocket-detail-sleeveless-shirt-dress/DZZ85180.html",
                    "rating": false,
                    "images": [
                        {
                            "src": "//i1.adis.ws/i/boohooamplience/dzz85180_honeydew_xl?$product_image_category_page$",
                            "alt": "Maeve Pocket Detail Sleeveless Shirt Dress"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 2000,
                    "wasPrice": false,
                    "title": "Carrie Cord Pocket Pinafore Dress",
                    "link": "http://www.boohoo.com/carrie-cord-pocket-pinafore-dress/DZZ89093.html",
                    "rating": false,
                    "images": [
                        {
                            "src": "//i1.adis.ws/i/boohooamplience/dzz89093_grey_xl?$product_image_category_page$",
                            "alt": "Carrie Cord Pocket Pinafore Dress"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 2000,
                    "wasPrice": false,
                    "title": "Jade Oversized Pocket Front Denim Dress",
                    "link": "http://www.boohoo.com/jade-oversized-pocket-front-denim-dress/DZZ93668.html",
                    "rating": false,
                    "images": [
                        {
                            "src": "//i1.adis.ws/i/boohooamplience/dzz93668_blue_xl?$product_image_category_page$",
                            "alt": "Jade Oversized Pocket Front Denim Dress"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 800,
                    "wasPrice": 1500,
                    "title": "Jasmine Wrap Skater Dress With Pockets",
                    "link": "http://www.boohoo.com/jasmine-wrap-skater-dress-with-pockets/DZZ53425.html",
                    "rating": false,
                    "images": [
                        {
                            "src": "//i1.adis.ws/i/boohooamplience/dzz53425_wine_xl?$product_image_category_page$",
                            "alt": "Jasmine Wrap Skater Dress With Pockets"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 1800,
                    "wasPrice": false,
                    "title": "Sally Side Pocket Denim Pinafore Dress",
                    "link": "http://www.boohoo.com/sally-side-pocket-denim-pinafore-dress/DZZ63136.html",
                    "rating": false,
                    "images": [
                        {
                            "src": "//i1.adis.ws/i/boohooamplience/dzz63136_mid%2520blue_xl?$product_image_category_page$",
                            "alt": "Sally Side Pocket Denim Pinafore Dress"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 800,
                    "wasPrice": 2200,
                    "title": "Natasha Faux Leather Pocket Pinafore Dress",
                    "link": "http://www.boohoo.com/natasha-faux-leather-pocket-pinafore-dress/DZZ72688.html",
                    "rating": false,
                    "images": [
                        {
                            "src": "//i1.adis.ws/i/boohooamplience/dzz72688_merlot_xl?$product_image_category_page$",
                            "alt": "Natasha Faux Leather Pocket Pinafore Dress"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 1200,
                    "wasPrice": 2000,
                    "title": "Plus Ella Lace Up Sleeve Pocket Detail Sweat Dress",
                    "link": "http://www.boohoo.com/plus-ella-lace-up-sleeve-pocket-detail-sweat-dress/PZZ90001.html",
                    "rating": false,
                    "images": [
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/pzz90001_cerise_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/pzz90001_cerise_xl?$product_image_category_page$",
                            "alt": "cerise",
                            "variantLink": "http://www.boohoo.com/plus-ella-lace-up-sleeve-pocket-detail-sweat-dress/PZZ90001.html?color=112"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/pzz90001_dusky%20pink_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/pzz90001_dusky%20pink_xl?$product_image_category_page$",
                            "alt": "dusky pink",
                            "variantLink": "http://www.boohoo.com/plus-ella-lace-up-sleeve-pocket-detail-sweat-dress/PZZ90001.html?color=355"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/pzz90001_lilac_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/pzz90001_lilac_xl?$product_image_category_page$",
                            "alt": "lilac",
                            "variantLink": "http://www.boohoo.com/plus-ella-lace-up-sleeve-pocket-detail-sweat-dress/PZZ90001.html?color=137"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 1200,
                    "wasPrice": 2000,
                    "title": "Alberta Utility Zip Front Pocket Shift Dress",
                    "link": "http://www.boohoo.com/alberta-utility-zip-front-pocket-shift-dress/DZZ62867.html",
                    "rating": false,
                    "images": [
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz62867_khaki_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz62867_khaki_xl?$product_image_category_page$",
                            "alt": "khaki",
                            "variantLink": "http://www.boohoo.com/alberta-utility-zip-front-pocket-shift-dress/DZZ62867.html?color=135"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz62867_black_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz62867_black_xl?$product_image_category_page$",
                            "alt": "black",
                            "variantLink": "http://www.boohoo.com/alberta-utility-zip-front-pocket-shift-dress/DZZ62867.html?color=105"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz62867_nude_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz62867_nude_xl?$product_image_category_page$",
                            "alt": "nude",
                            "variantLink": "http://www.boohoo.com/alberta-utility-zip-front-pocket-shift-dress/DZZ62867.html?color=295"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 600,
                    "wasPrice": 1500,
                    "title": "Alice V Neck Pocket Front Shift Dress",
                    "link": "http://www.boohoo.com/alice-v-neck-pocket-front-shift-dress/DZZ66488.html",
                    "rating": false,
                    "images": [
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz66488_khaki_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz66488_khaki_xl?$product_image_category_page$",
                            "alt": "khaki",
                            "variantLink": "http://www.boohoo.com/alice-v-neck-pocket-front-shift-dress/DZZ66488.html?color=135"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz66488_black_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz66488_black_xl?$product_image_category_page$",
                            "alt": "black",
                            "variantLink": "http://www.boohoo.com/alice-v-neck-pocket-front-shift-dress/DZZ66488.html?color=105"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 1200,
                    "wasPrice": 2000,
                    "title": "Hollie Pocket Shirt Dress",
                    "link": "http://www.boohoo.com/hollie-pocket-shirt-dress/DZZ54040.html",
                    "rating": false,
                    "images": [
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz54040_pastel%20blue_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz54040_pastel%20blue_xl?$product_image_category_page$",
                            "alt": "pastel blue",
                            "variantLink": "http://www.boohoo.com/hollie-pocket-shirt-dress/DZZ54040.html?color=396"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz54040_white_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz54040_white_xl?$product_image_category_page$",
                            "alt": "white",
                            "variantLink": "http://www.boohoo.com/hollie-pocket-shirt-dress/DZZ54040.html?color=173"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 800,
                    "wasPrice": 2200,
                    "title": "Debbie Cross Front Pocket Detail Bodycon Dress",
                    "link": "http://www.boohoo.com/debbie-cross-front-pocket-detail-bodycon-dress/DZZ86716.html",
                    "rating": false,
                    "images": [
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz86716_taupe_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz86716_taupe_xl?$product_image_category_page$",
                            "alt": "taupe",
                            "variantLink": "http://www.boohoo.com/debbie-cross-front-pocket-detail-bodycon-dress/DZZ86716.html?color=167"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz86716_black_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz86716_black_xl?$product_image_category_page$",
                            "alt": "black",
                            "variantLink": "http://www.boohoo.com/debbie-cross-front-pocket-detail-bodycon-dress/DZZ86716.html?color=105"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 1900,
                    "wasPrice": 2000,
                    "title": "Laura Zip Through Patch Pocket Denim Pinafore Dress",
                    "link": "http://www.boohoo.com/laura-zip-through-patch-pocket-denim-pinafore-dress/DZZ63137.html",
                    "rating": false,
                    "images": [
                        {
                            "src": "//i1.adis.ws/i/boohooamplience/dzz63137_blue_xl?$product_image_category_page$",
                            "alt": "Laura Zip Through Patch Pocket Denim Pinafore Dress"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 1800,
                    "wasPrice": 2000,
                    "title": "Claire Fray Patch Pocket Pinafore Denim",
                    "link": "http://www.boohoo.com/claire-fray-patch-pocket-pinafore-denim/DZZ46837.html",
                    "rating": false,
                    "images": [
                        {
                            "src": "//i1.adis.ws/i/boohooamplience/dzz46837_blue_xl?$product_image_category_page$",
                            "alt": "Claire Fray Patch Pocket Pinafore Denim"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 1400,
                    "wasPrice": 2000,
                    "title": "Lisa Frayed Pocket Denim Pinafore Dress",
                    "link": "http://www.boohoo.com/lisa-frayed-pocket-denim-pinafore-dress/DZZ51581.html",
                    "rating": false,
                    "images": [
                        {
                            "src": "//i1.adis.ws/i/boohooamplience/dzz51581_blue_xl?$product_image_category_page$",
                            "alt": "Lisa Frayed Pocket Denim Pinafore Dress"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 1000,
                    "wasPrice": 3500,
                    "title": "Boutique Liz Suedette Eyelet Pocket Detail Dress",
                    "link": "http://www.boohoo.com/boutique-liz-suedette-eyelet-pocket-detail-dress/DZZ86901.html",
                    "rating": false,
                    "images": [
                        {
                            "src": "//i1.adis.ws/i/boohooamplience/dzz86901_chestnut_xl?$product_image_category_page$",
                            "alt": "Boutique Liz Suedette Eyelet Pocket Detail Dress"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 800,
                    "wasPrice": 1800,
                    "title": "Callie Plunge Neck Pocket Detail Bodycon Dress",
                    "link": "http://www.boohoo.com/callie-plunge-neck-pocket-detail-bodycon-dress/DZZ92387.html",
                    "rating": false,
                    "images": [
                        {
                            "src": "//i1.adis.ws/i/boohooamplience/dzz92387_spice_xl?$product_image_category_page$",
                            "alt": "Callie Plunge Neck Pocket Detail Bodycon Dress"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 1200,
                    "wasPrice": 1800,
                    "title": "Eleanor Pocket Sweat Dress",
                    "link": "http://www.boohoo.com/eleanor-pocket-sweat-dress/DZZ61395.html",
                    "rating": false,
                    "images": [
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz61395_antique%20rose_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz61395_antique%20rose_xl?$product_image_category_page$",
                            "alt": "antique rose",
                            "variantLink": "http://www.boohoo.com/eleanor-pocket-sweat-dress/DZZ61395.html?color=434"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz61395_black_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz61395_black_xl?$product_image_category_page$",
                            "alt": "black",
                            "variantLink": "http://www.boohoo.com/eleanor-pocket-sweat-dress/DZZ61395.html?color=105"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz61395_rust_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz61395_rust_xl?$product_image_category_page$",
                            "alt": "rust",
                            "variantLink": "http://www.boohoo.com/eleanor-pocket-sweat-dress/DZZ61395.html?color=208"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 1000,
                    "wasPrice": 2000,
                    "title": "Maia Cotton Shirt Dress With Pockets",
                    "link": "http://www.boohoo.com/maia-cotton-shirt-dress-with-pockets/DZZ58935.html",
                    "rating": false,
                    "images": [
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz58935_black_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz58935_black_xl?$product_image_category_page$",
                            "alt": "black",
                            "variantLink": "http://www.boohoo.com/maia-cotton-shirt-dress-with-pockets/DZZ58935.html?color=105"
                        },
                        {
                            "thumb": "//i1.adis.ws/i/boohooamplience/dzz58935_blue_xl_s.jpg?$product_image_swatch$",
                            "src": "//i1.adis.ws/i/boohooamplience/dzz58935_blue_xl?$product_image_category_page$",
                            "alt": "blue",
                            "variantLink": "http://www.boohoo.com/maia-cotton-shirt-dress-with-pockets/DZZ58935.html?color=106"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 1200,
                    "wasPrice": 2500,
                    "title": "Ria Chambray Frill Pocket Oversized Shirt Dress",
                    "link": "http://www.boohoo.com/ria-chambray-frill-pocket-oversized-shirt-dress/DZZ61556.html",
                    "rating": false,
                    "images": [
                        {
                            "src": "//i1.adis.ws/i/boohooamplience/dzz61556_blue_xl?$product_image_category_page$",
                            "alt": "Ria Chambray Frill Pocket Oversized Shirt Dress"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 1700,
                    "wasPrice": 3000,
                    "title": "Plus Lou Embroidered Pocket Denim Pinafore Dress",
                    "link": "http://www.boohoo.com/plus-lou-embroidered-pocket-denim-pinafore-dress/PZZ91539.html",
                    "rating": false,
                    "images": [
                        {
                            "src": "//i1.adis.ws/i/boohooamplience/pzz91539_mid%2520blue_xl?$product_image_category_page$",
                            "alt": ""
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 800,
                    "wasPrice": 1600,
                    "title": "Julia Oversized Slouchy Pocket T-Shirt Dress",
                    "link": "http://www.boohoo.com/julia-oversized-slouchy-pocket-t-shirt-dress/DZZ68950.html",
                    "rating": false,
                    "images": [
                        {
                            "src": "//i1.adis.ws/i/boohooamplience/dzz68950_grey_xl?$product_image_category_page$",
                            "alt": "Julia Oversized Slouchy Pocket T-Shirt Dress"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 600,
                    "wasPrice": 1500,
                    "title": "Kathy Pocket Front Shift Dress",
                    "link": "http://www.boohoo.com/kathy-pocket-front-shift-dress/DZZ60350.html",
                    "rating": false,
                    "images": [
                        {
                            "src": "//i1.adis.ws/i/boohooamplience/dzz60350_black_xl?$product_image_category_page$",
                            "alt": "Kathy Pocket Front Shift Dress"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 1500,
                    "wasPrice": false,
                    "title": "Fran Crepe Plunge Neck Pocket Shift Dress",
                    "link": "http://www.boohoo.com/fran-crepe-plunge-neck-pocket-shift-dress/DZZ51642.html",
                    "rating": false,
                    "images": [
                        {
                            "src": "//i1.adis.ws/i/boohooamplience/dzz51642_blush_xl?$product_image_category_page$",
                            "alt": "Fran Crepe Plunge Neck Pocket Shift Dress"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                },
                {
                    "vendor": "boohoo",
                    "price": 600,
                    "wasPrice": 1500,
                    "title": "Genevieve Satin Pocket Shift Dress",
                    "link": "http://www.boohoo.com/genevieve-satin-pocket-shift-dress/DZZ63492.html",
                    "rating": false,
                    "images": [
                        {
                            "src": "//i1.adis.ws/i/boohooamplience/dzz63492_silver_xl?$product_image_category_page$",
                            "alt": "Genevieve Satin Pocket Shift Dress"
                        }
                    ],
                    "descriptionHtml": false,
                    "categories": [
                        "dresses"
                    ]
                }
            ]);
        });

    });

    it("throws an error if the initial read errors", () => {
        const error = {
            isError: true
        };
        mockRequest.mock.setResponse("get", error, null, null);

        return getBoohoo()
            .then(failTest)
            .catch((returnedError) => {
                expect(returnedError).to.equal(error);
            });

    });

    it("throws an error if the read produces no valid products", () => {
        const mockPageSnippet = `<div id="search-result-items"></div>`;

        mockRequest.mock.setResponse("get", null, null, mockPageSnippet);

        return getBoohoo()
            .then(failTest)
            .catch((returnedError) => {
                expect(returnedError).to.deep.equal("No valid products were found");
            })
    });
});