import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { NgxPaginationModule } from 'ngx-pagination';

import { ProductService } from '../../product-service/product.service';
import { ProductSearchComponent } from './product-search.component';
import { ProductListComponent } from '../../product-list/product-list.component';
import { ProductCardComponent } from '../../product-card/product-card.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('ProductSearchComponent', () => {
  let component: ProductSearchComponent;
  let fixture: ComponentFixture<ProductSearchComponent>;

  const mockProductList = [];
  class MockProductService {
    getProductList = jasmine.createSpy('getProductList').and.returnValue(Observable.of(mockProductList));
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule, NgxPaginationModule],
      providers: [{ provide: ProductService, useClass: MockProductService }],
      declarations: [ProductSearchComponent, ProductListComponent, ProductCardComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
