import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'igp-accordion-row',
  templateUrl: './igp-accordion-row.component.html',
  styleUrls: ['./igp-accordion-row.component.scss']
})
export class IgpAccordionRowComponent implements OnInit {
  @Input() title;
  @Input() titleClass;
  @Input() isOpen;
  @Input() maxheight: string = "500";

  constructor() { }

  ngOnInit() {
    this.maxheight += "px";
  }
  toggleStatus() {
    this.isOpen = !this.isOpen;
  }

}
