const chai = require('chai')
    .use(require('sinon-chai'));

const sinon = require('sinon');
const expect = chai.expect;
const proxyquire = require("proxyquire");

const putObjectSpy = sinon.spy();
const getObjectSpy = sinon.stub();
const AWS = require("aws-sdk-mock");
let s3Response;
let s3Error;

AWS.mock("S3", "putObject", (params, callback) => {
    putObjectSpy(params);
    callback(s3Error, s3Response);
});

AWS.mock("S3", "getObject", (params, callback) => {
    getObjectSpy(params, callback);
});

const storage = proxyquire('./storage', {
    "aws-sdk": AWS
});

describe("storage", () => {
    const mockBucketName = "Bucket!";

    beforeEach(() => {
        process.env.BucketName = mockBucketName;
    });
    afterEach(() => {
        putObjectSpy.reset();
        getObjectSpy.reset();
        s3Response = null;
        s3Error = null;
    });

    describe("putting", () => {
        it("can put JSON into s3", () => {
            s3Response = "Success!";
            return storage.put("filename", {
                myData: true
            }).then((resp) => {
                expect(putObjectSpy).calledWith({
                    Key: "filename",
                    Bucket: mockBucketName,
                    Body: new Buffer(JSON.stringify({
                        myData: true
                    }), 'utf-8')
                });
            });
        });

        it("can put strings into s3", () => {
            s3Response = "Success!";
            return storage.put("filename", "filedata").then((resp) => {
                expect(putObjectSpy).calledWith({
                    Key: "filename",
                    Bucket: mockBucketName,
                    Body: new Buffer("filedata", "utf-8")
                });
            });
        });

        it("throws s3 errors if they are returned", () => {
            s3Error = "Fail!";
            return storage.put("filename", "filedata").then(() => {
                    expect(true).to.equal(false, "Should not have successfully put");
                })
                .catch((error) => {
                    expect(error).to.equal("Fail!");
                });
        });
    });

    describe("getting", () => {
        it("can get JSON out of s3", () => {
            const getResponse = {
                Body: new Buffer(JSON.stringify({
                    myData: true
                }))
            };
            getObjectSpy.callsArgWith(1, null, getResponse);
            return storage.getJSON("filename").then((resp) => {
                expect(resp).to.deep.equal({
                    myData: true
                });
            });
        });

        it("throws s3 errors if they are returned", () => {
            const getError = "Getting didn't work";
            getObjectSpy.callsArgWith(1, getError);

            return Promise.all([
                storage.getJSON("filename").catch((error) => expect(error).to.equal(getError))
            ]);
        });
    });
});