import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ProductFilters } from '../product-filter/ProductFilters';
import { ProductService } from '../product-service/product.service';

@Injectable()
export class ProductBrowserService {
  private sortOrder: string;
  private filters: ProductFilters;
  private page: number;
  private pageSize: number;

  constructor(private router: Router, private productService: ProductService) { }

  private update() {
    let queryParams: any = {};
    if (this.filters) {
      const filters = Object.assign({}, this.filters);
      if (filters.categories) {
        filters.categories = filters.categories.map((category) => {
          return category.key;
        });
      }

      if (filters.flags) {
        filters.flags = Object.keys(filters.flags).filter(flag => {
          return filters.flags[flag];
        });
      }

      Object.keys(filters).forEach((keyName) => {
        const filter = filters[keyName];
        if (!filter || (Array.isArray(filter) && !filter.length)) delete filters[keyName];
      });

      queryParams = filters;
    }

    if (this.sortOrder) queryParams.sort = this.sortOrder;
    if (this.page) queryParams.page = this.page;


    this.router.navigate([], { queryParams });
  }
  private asArray(param) {
    if (!param) return [];
    return Array.isArray(param) ? param : [param];
  }


  init(queryParams?) {
    if (!queryParams || !Object.keys(queryParams).length) {
      this.filters = {
        categories: [],
        keywords: [],
        maxPrice: undefined,
        minPrice: undefined,
        flags: {}
      };
      this.page = 1;
      this.sortOrder = '';
      return;
    }

    this.page = queryParams.page || 1;
    this.pageSize = queryParams.pageSize || 12;
    this.sortOrder = queryParams.sort || "";
    this.filters = {
      minPrice: parseFloat(queryParams.minPrice),
      maxPrice: parseFloat(queryParams.maxPrice),
      keywords: queryParams.keywords ? this.asArray(queryParams.keywords) : [],
      categories: this.asArray(queryParams.categories).map(categoryKey => {
        return this.productService.getCategory(categoryKey);
      }),
      flags: this.asArray(queryParams.flags).reduce((flags, flagName) => {
        flags[flagName] = true;
        return flags;
      }, {})
    }

    if (isNaN(this.filters.maxPrice)) this.filters.maxPrice = undefined;
    if (isNaN(this.filters.minPrice)) this.filters.minPrice = undefined;
  }

  setSortOrder(sortOrder) {
    this.sortOrder = sortOrder;
    this.update();
  }
  getSortOrder() {
    return this.sortOrder;
  }

  setFilters(filters: ProductFilters) {
    this.filters = filters;
    this.update();
  }
  getFilters() {
    return this.filters;
  }

  setPage(pageNumber) {
    this.page = pageNumber;
    this.update();
  }
  getPage() {
    return this.page;
  }
  getPageSize() {
    return this.pageSize;
  }

}
