import { Component, OnInit, Input } from '@angular/core';
import { ProductBrowserService } from '../product-browser/product-browser.service';
import { ProductFilters } from './ProductFilters';

@Component({
  selector: 'product-filter',
  templateUrl: './product-filter.component.html',
  styleUrls: ['./product-filter.component.scss']
})
export class ProductFilterComponent implements OnInit {
  @Input() filterQuery;
  public categories = [
    { label: "Dresses", key: "dresses" }
  ]
  public newKeywordString = "";
  public priceRange = "-";
  public isAboveTablet = (window.innerWidth > 768);
  public errors: any = {};

  public productFilters: ProductFilters;

  constructor(private productBrowserService: ProductBrowserService) { }

  ngOnInit() {
    this.productFilters = this.productBrowserService.getFilters();
    this.selectPriceRangeType(this.productFilters.minPrice, this.productFilters.maxPrice);

  }

  private updateKeywords(removeWord, addWord?) {
    this.productFilters.keywords = this.productFilters.keywords.filter(keyword => keyword.toLowerCase() !== removeWord.toLowerCase());
    if (addWord) this.productFilters.keywords.push(addWord);
  }
  updateFilters() {
    this.productBrowserService.setFilters(this.productFilters);
  }
  removeKeyword(removeWord) {
    this.updateKeywords(removeWord);
    this.updateFilters()
  }
  addKeywords() {
    if (!this.newKeywordString) return;
    const newKeywords = this.newKeywordString.trim().split(" ");
    this.newKeywordString = '';

    newKeywords.forEach(addWord => {
      this.updateKeywords(addWord, addWord);
    });
    this.updateFilters()
  }
  detectKeywordSubmission(event) {
    if (event.keyCode === 13) {
      this.addKeywords();
    }
  }
  clearKeywords() {
    this.productFilters.keywords = [];
    this.updateFilters()
  }

  selectPriceRangeType(min, max) {
    if (!max && !min) this.priceRange = "";
    else if (max === 50 && min === 20) this.priceRange = "20-50";
    else if (max === 20 && !min) this.priceRange = "-20";
    else if (!max && min === 50) this.priceRange = "50-";
    else this.priceRange = "custom";
    this.updateFilters()
  }
  setPriceRange(min, max) {
    this.productFilters.minPrice = min;
    this.productFilters.maxPrice = max;
    this.updateFilters()
  }
}
