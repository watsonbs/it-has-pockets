import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../Product';

import 'rxjs/add/operator/first';

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  @Input() productList: Product[];
  @Input() currentPage: number = 1;
  @Input() pageSize: number = 12;

  constructor() { }

  ngOnInit() {
  }

  fixHeight(event) {
    const currentHeight = getComputedStyle(event.target).height;
    event.target.style.height = currentHeight;
    event.target.classList.add("hover");
  }
  releaseHeight(event) {
    event.target.style = '';
    event.target.classList.remove("hover");
  }

}
