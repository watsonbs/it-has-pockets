import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutSiteComponent } from './about-site/about-site.component';
import { ContactSiteComponent } from './contact-site/contact-site.component';

const routes: Routes = [
    { path: '', redirectTo: "about" },
    { path: 'about', component: AboutSiteComponent },
    { path: 'contact', component: ContactSiteComponent },
];


export const routing: ModuleWithProviders = RouterModule.forChild(routes);