const sinon = require("sinon");

const mockRequest = {
    get: sinon.stub(),
    mock: {
        setResponse: (method, error, response, body) => {
            mockRequest[method].callsArgWith(1, error, response, body);
        },
        reset: () => {
            mockRequest.get.reset();
        }
    }
};

module.exports = mockRequest;