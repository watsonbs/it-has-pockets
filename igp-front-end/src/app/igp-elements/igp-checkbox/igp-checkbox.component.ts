import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'igp-checkbox',
  templateUrl: './igp-checkbox.component.html',
  styleUrls: ['./igp-checkbox.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => IgpCheckBoxComponent),
      multi: true
    }
  ]
})
export class IgpCheckBoxComponent implements OnInit, ControlValueAccessor {
  @Input() name: string;
  @Input() label: string;
  @Input() value: string;
  @Input() disabled: boolean = false;

  private propagateChange = (_: any) => { };
  private propagateTouched = (_: any) => { };
  public isChecked = false;


  constructor() { }

  ngOnInit() {
  }

  writeValue(value: any) {
    this.isChecked = Boolean(value);
  }
  registerOnChange(fn) { this.propagateChange = fn }
  registerOnTouched(fn) { this.propagateTouched = fn }
  onChange(event) {
    this.propagateChange(event.target.checked);
  }
}
