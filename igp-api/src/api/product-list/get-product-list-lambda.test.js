const chai = require('chai')
    .use(require('sinon-chai'));

const sinon = require('sinon');
const expect = chai.expect;
const proxyquire = require("proxyquire");

const mockStorage = {
    getJSON: sinon.stub()
};

const getProductListLambda = proxyquire("./get-product-list-lambda", {
    "../../utilities/storage": mockStorage
});

describe("get-product-list-lambda", () => {
    afterEach(() => {
        mockStorage.getJSON.reset();
    });

    it("can return the product list from s3", () => {
        const callbackSpy = sinon.spy();
        mockStorage.getJSON.returns(Promise.resolve({
            myData: true
        }));
        return getProductListLambda
            .get({}, {}, callbackSpy)
            .then(() => {
                expect(callbackSpy).calledWith(null, {
                    "statusCode": 200,
                    "headers": {
                        "Cache-Control": "private, max-age=86400000",
                        "Content-Type": "application/json"
                    },
                    "body": {
                        "data": {
                            myData: true
                        }
                    }
                });
            });
    });

    it("returns an error if it is unable to read the data", () => {
        const callbackSpy = sinon.spy();
        mockStorage.getJSON.returns(Promise.reject("cannot get"));
        return getProductListLambda
            .get({}, {}, callbackSpy)
            .then(() => {
                expect(callbackSpy).calledWith("cannot get");
            });
    });
});