import { TestBed, inject } from '@angular/core/testing';
import { Router } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';
import { Observable } from 'rxjs/Observable';

import { ProductService } from '../product-service/product.service';

import { ProductBrowserService } from './product-browser.service';

describe('ProductBrowserService', () => {
  const mockRouter = jasmine.createSpyObj('router', ['navigate']);
  const mockProductService = jasmine.createSpyObj('productService', ['getCategory'])
  const filters = {
    categories: [{ key: 'dresses', label: "Dresses" }],
    minPrice: 10,
    maxPrice: 100,
    keywords: ["test", "toast"]
  };

  const mockProductServiceFactory = () => {
    return mockProductService;
  };
  const mockRouterFactory = () => {
    return mockRouter;
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NgxPaginationModule],
      providers: [
        ProductBrowserService,
        { provide: ProductService, useFactory: mockProductServiceFactory },
        { provide: Router, useFactory: mockRouterFactory }
      ]
    });
  });

  afterEach(() => {
    mockRouter.navigate.calls.reset();
  });

  describe("initialisation", () => {
    it("can iniitalise its own values given a set of query parameters", inject([ProductBrowserService], (service: ProductBrowserService) => {
      mockProductService.getCategory.and.returnValue({ key: 'dresses', label: "Dresses" });
      service.init({
        sort: "price_d",
        page: 2,
        categories: 'dresses',
        keywords: ['test', 'toast'],
        minPrice: 10
      });
      expect(service.getPage()).toBe(2);
      expect(service.getSortOrder()).toBe("price_d");
      expect(service.getFilters()).toEqual({
        categories: [{ key: 'dresses', label: 'Dresses' }],
        keywords: ['test', 'toast'],
        minPrice: 10,
        maxPrice: undefined,
        flags: {}
      });
    }));

    it("returns the default values given no query parameters", inject([ProductBrowserService], (service: ProductBrowserService) => {
      service.init();
      expect(service.getPage()).toBe(1);
      expect(service.getSortOrder()).toBe('');
      expect(service.getFilters()).toEqual({
        categories: [],
        keywords: [],
        maxPrice: undefined,
        minPrice: undefined,
        flags: {}
      });
    }));
  });


  describe("sorting products", () => {
    it('can update the query parameters to refelect sort order', inject([ProductBrowserService], (service: ProductBrowserService) => {
      service.setSortOrder("price_d");
      expect(mockRouter.navigate).toHaveBeenCalledWith(["."], { queryParams: { sort: "price_d" } });
    }));

    it("does not remove existing params when setting the sort order", inject([ProductBrowserService], (service: ProductBrowserService) => {

      service.setPage(10);
      service.setFilters(filters);
      expect(mockRouter.navigate).toHaveBeenCalledWith(["."], {
        queryParams: {
          categories: ["dresses"],
          minPrice: 10,
          maxPrice: 100,
          keywords: ["test", "toast"],
          page: 10
        }
      });

      service.setSortOrder("price_d");
      expect(mockRouter.navigate).toHaveBeenCalledWith(["."], {
        queryParams: {
          categories: ["dresses"],
          minPrice: 10,
          maxPrice: 100,
          keywords: ["test", "toast"],
          sort: "price_d",
          page: 10
        }
      });
      expect(mockRouter.navigate).toHaveBeenCalledTimes(3);
    }));

    it("can get the current sort order", inject([ProductBrowserService], (service: ProductBrowserService) => {
      service.setSortOrder("price_d");
      expect(service.getSortOrder()).toBe("price_d");
    }));
  });

  describe("filtering products", () => {
    it("can update the query parameters to refelect the filter", inject([ProductBrowserService], (service: ProductBrowserService) => {
      service.setFilters(filters);
      expect(mockRouter.navigate).toHaveBeenCalledWith(["."], {
        queryParams: {
          categories: ["dresses"],
          minPrice: 10,
          maxPrice: 100,
          keywords: ["test", "toast"]
        }
      });
    }));

    it("does not remove existing params when setting the filters", inject([ProductBrowserService], (service: ProductBrowserService) => {
      service.setSortOrder("price_d");
      service.setPage(10);
      expect(mockRouter.navigate).toHaveBeenCalledWith(["."], {
        queryParams: {
          page: 10,
          sort: "price_d"
        }
      });

      service.setFilters(filters);
      expect(mockRouter.navigate).toHaveBeenCalledWith(["."], {
        queryParams: {
          categories: ["dresses"],
          minPrice: 10,
          maxPrice: 100,
          keywords: ["test", "toast"],
          sort: "price_d",
          page: 10
        }
      });
      expect(mockRouter.navigate).toHaveBeenCalledTimes(3);
    }));

    it("can get the current filters", inject([ProductBrowserService], (service: ProductBrowserService) => {
      service.setFilters(filters);
      expect(service.getFilters()).toBe(filters);
    }));

    it("remaps flags to query params", inject([ProductBrowserService], (service: ProductBrowserService) => {
      const flagFilters = {
        flags: {
          isDiscounted: true,
          isFakeFlag: false
        }
      };
      service.setFilters(flagFilters);
      expect(service.getFilters()).toBe(flagFilters);
      expect(mockRouter.navigate).toHaveBeenCalledWith(["."], {
        queryParams: {
          flags: ['isDiscounted']
        }
      });
    }));
  });

  describe("pagination", () => {
    it("can update the query parameters to refelect the pagination", inject([ProductBrowserService], (service: ProductBrowserService) => {
      service.setPage(10);
      expect(mockRouter.navigate).toHaveBeenCalledWith(["."], {
        queryParams: {
          page: 10
        }
      });
    }));

    it("does not remove existing params when setting the page", inject([ProductBrowserService], (service: ProductBrowserService) => {
      service.setSortOrder("price_d");
      service.setFilters(filters);
      expect(mockRouter.navigate).toHaveBeenCalledWith(["."], {
        queryParams: {
          categories: ["dresses"],
          minPrice: 10,
          maxPrice: 100,
          keywords: ["test", "toast"],
          sort: "price_d"
        }
      });

      service.setPage(10);
      expect(mockRouter.navigate).toHaveBeenCalledWith(["."], {
        queryParams: {
          categories: ["dresses"],
          minPrice: 10,
          maxPrice: 100,
          keywords: ["test", "toast"],
          sort: "price_d",
          page: 10
        }
      });
      expect(mockRouter.navigate).toHaveBeenCalledTimes(3);
    }));

    it("can get the current page", inject([ProductBrowserService], (service: ProductBrowserService) => {
      service.setPage(5);
      expect(service.getPage()).toBe(5);
    }));
  });

});
