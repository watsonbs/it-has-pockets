import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductHomeComponent } from './product-display/pages/product-home/product-home.component';
import { ProductSearchComponent } from './product-display/pages/product-search/product-search.component';

const routes: Routes = [
    { path: '', component: ProductHomeComponent },
    { path: 'search/:searchTerm', component: ProductSearchComponent },
    { path: 'meta', loadChildren: './site-meta/site-meta.module#SiteMetaModule' }
];


export const routing: ModuleWithProviders = RouterModule.forRoot(routes);