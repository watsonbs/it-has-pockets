const getBoden = require('../product-scrapers/get-boden/get-boden');
const getBoohoo = require('../product-scrapers/get-boohoo/get-boohoo');
const logger = require("../../utilities/logger");

module.exports = () => {
    return Promise.all([
        getBoohoo().catch((error) => {
            logger.error("boohoo", error)
        }),
        getBoden().catch((error) => {
            logger.error("boden", error)
        })
    ]).then((productData) => {
        let products = [];
        productData.forEach((shopProducts) => {
            if (shopProducts) {
                products = products.concat(...shopProducts);
            }
        });

        return products;
    });
};