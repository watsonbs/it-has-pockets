import { Component, OnInit } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

import 'rxjs/add/operator/map';

@Component({
  selector: 'contact-site',
  templateUrl: './contact-site.component.html',
  styleUrls: ['./contact-site.component.scss']
})
export class ContactSiteComponent implements OnInit {
  public emailSubject;
  public isMailSent = false;
  public submissionError = "";

  constructor(private http: Http) { }

  public sendMail(form) {
    const headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http.post("https://formspree.io/contact@itsgotpockets.co.uk",
      {
        name: form.subject,
        _replyto: form.email,
        message: form.message
      },
      {
        'headers': headers
      }
    )
      .first()
      .map(res => res.json())
      .subscribe((resp) => {
        if (resp.success) {
          this.isMailSent = true;
        } else {
          this.submissionError = "Something went wrong. Please try again later.";
        }
      });
  }

  ngOnInit() {
  }

}
