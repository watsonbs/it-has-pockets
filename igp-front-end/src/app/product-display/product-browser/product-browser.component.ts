import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Product } from '../Product';
import { ProductService } from '../product-service/product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductBrowserService } from './product-browser.service';
import { ObjectUtilityService } from '../../utilities/object-utility/object-utility.service';

import { Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'product-browser',
  templateUrl: './product-browser.component.html',
  styleUrls: ['./product-browser.component.scss']
})
export class ProductBrowserComponent implements OnInit, OnDestroy {
  @Input() searchContext?: any;
  public productList: Product[];
  public filterQuery;
  public currentPage: number;
  public pageSize: number;
  public sortOrder: string;
  private filterSubscription: Subscription;
  constructor(
    private productService: ProductService,
    private productBrowserService: ProductBrowserService,
    private router: Router,
    private route: ActivatedRoute,
    private objectUtility: ObjectUtilityService
  ) { }

  updateSortOrder() {
    this.productBrowserService.setSortOrder(this.sortOrder);
  }
  changePage(pageNumber) {
    this.productBrowserService.setPage(pageNumber);
  }
  ngOnInit() {

    this.filterSubscription = this.route.queryParams.subscribe((queryParams) => {
      this.productBrowserService.init(queryParams);
      this.filterQuery = this.productBrowserService.getFilters();
      this.currentPage = this.productBrowserService.getPage();
      this.pageSize = this.productBrowserService.getPageSize();
      this.sortOrder = this.productBrowserService.getSortOrder();

      const searchParams = this.searchContext ? this.objectUtility.recursiveMerge(this.filterQuery, this.searchContext) : this.filterQuery;

      this.productService.getProductList(searchParams, this.sortOrder).first().subscribe(
        (productList) => {
          this.productList = productList;
        },
        (error) => console.error("biffed", error)
      )
    });

  }

  ngOnDestroy() {
    this.filterSubscription.unsubscribe();
  }

}
